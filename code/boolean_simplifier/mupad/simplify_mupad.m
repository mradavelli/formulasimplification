function simplify_mupad()
tempDir = '../temp/';
inputFile = 'mupad_in.txt';
outputFile = 'mupad_out.txt';
    
fid = fopen(strcat(tempDir,inputFile), 'r');
tline = fgets(fid);
fclose(fid);
t = datetime('now');
s = simplify(tline);
elapsed = round(milliseconds(datetime('now')-t));
fidw= fopen(strcat(tempDir,outputFile),'w');
fprintf(fidw,'%d\t%s',elapsed,s);
fclose(fidw);
display(sprintf('finished\t%s',s));
end

function y = simplify(s)
s = strrep(s, '&&', ' and ');
s = strrep(s, '||', ' or ');
s = strrep(s, '!', 'not ');
y = feval(symengine,'simplify',s,'logic');
end