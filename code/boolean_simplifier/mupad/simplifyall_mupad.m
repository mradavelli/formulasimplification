function simplifyall_mupad()
inputdir = '../input/';
outputdir = '../output/';
files = cellstr({'busyboxNestedIfDefConstraints';
    'busyboxParserErrorConstraints';
    'busyboxTypeErrorConstraints';'ecosParserErrorConstraints';
    'ecosPreprocessorErrors';
    'ecosTypeErrorConstraints';
    'linuxDefUseConstraints';
    'linuxParserErrorConstraints';
    'uclibcTypeErrorConstraints';
    'tacasSpecs';
    'toughestRandomSpecs'});
simplify_all(strcat(inputdir,files,'.txt'),strcat(outputdir,files,'_MuPAD.txt'));
end

function simplify_all(inputFile, outputFile)
for k=1:length(inputFile) 
    disp(strcat('Simplifying ',inputFile{k}))
    fid = fopen(inputFile{k}, 'r');
    fidw= fopen(outputFile{k},'w');
    tline = fgets(fid);
    i=1;
    while ischar(tline)
        t = datetime('now');
        s = simplify(tline);
        elapsed = round(milliseconds(datetime('now')-t));
        if (elapsed <= 10000)
            fprintf(fidw,'%d\t%d\t0\t%s\n',i,elapsed,s);
        else
            fprintf(fidw,'%d\t%d\t0\t%s\n',i,10000,tline);
        end
        i=i+1;
        tline = fgets(fid);
        if mod(i,10) == 0
            disp(i)
        end
    end
    fclose(fid);
    fclose(fidw);
end
end

function y = simplify(s)
s = strrep(s, '&&', ' and ');
s = strrep(s, '||', ' or ');
s = strrep(s, '!', 'not ');
s = strrep(s, 'definedEx','');
y = feval(symengine,'simplify',s,'logic');
end