import pyeda
from pyeda.inter import *
#a, b, c = map(bddvar, 'abc')
#f = a & b | a & c | b & c
f = expr(sys.argv[2])
f, = espresso_exprs(f.to_dnf()) # f=f.simplify() does not work
f = expr2bdd(f)
s=f.to_dot()
with open("bdd.dot", "w") as text_file:
    print(s, file=text_file)
from subprocess import call
call("dot -Tpng bdd.dot > bdd.png", shell=True)