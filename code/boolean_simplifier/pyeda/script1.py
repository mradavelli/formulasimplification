import pyeda
from pyeda.inter import *
#a, b, c = map(bddvar, 'abc')
#f = a & b | a & c | b & c
f = expr("~(a & b) & ~(a & b) & ((d & ~e & ~f) | (~d & e & ~f) | (~d & ~e & ~f)) & (a & c & (d | e) & h | a & (d | e) & ~h | b & (e | f ))")
print(f)
f, = espresso_exprs(f.to_dnf()) # f=f.simplify() does not work
print(f)
f = expr2bdd(f)
s=f.to_dot()
with open("input.dot", "w") as text_file:
    print(s, file=text_file)
from subprocess import call
call("dot -Tpng input.dot > output.png", shell=True)