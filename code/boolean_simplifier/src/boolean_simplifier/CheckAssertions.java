package boolean_simplifier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import boolean_simplifier.methods.AbstractSimplifier;
import boolean_simplifier.methods.TXL;

/** Check the assetions from the TXL Debug on the formulas pointed out by CHECK_FILENAME */
public class CheckAssertions {

	public static void main(String[] args) {
		checkFormulas();
	}
	
	private static final String CHECK_FILENAME = "formulasToCheck";
	
	private static void checkFormulas() {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(LaunchSimplifier.INPUT_FOLDER+CHECK_FILENAME+".txt"));
			String s="";
			while ((s=fin.readLine())!=null) if (s.contains(" ")) {
				checkFormula(s.split(" ")[0], Integer.parseInt(s.split(" ")[1]));
			}
			fin.close();
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private static void checkFormula(String filename, int line) {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(LaunchSimplifier.INPUT_FOLDER+filename+".txt"));
			String s="";
			for (int i=1; (s=fin.readLine())!=null; i++) if (i==line) {
				checkTXLOutput(getTXLOutput(new Formula(s)));
				break;
			}
			fin.close();
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private static void checkTXLOutput(String txlOutput) {
		String[] lines = txlOutput.substring(txlOutput.lastIndexOf("...")+3).replace("\n","").split("]");
		for (int i=1; i<=lines.length; i++) {
			String s = lines[i-1];
			s = s.replace("-","!");
			while (s.contains("! !")) s=s.replace("! !", "");
			while (s.contains("!!")) s=s.replace("!!", "");
			if (!s.contains("==>")) continue;
			String s1 = s.substring(0, s.indexOf("==>"));
			String s2 = s.substring(s.indexOf("==>")+4,s.indexOf("["));
			String rule = s.substring(s.indexOf('['));
			Formula f = new Formula(s1);
			System.out.println(i+"\t"+f.checkEquivalence(s2)+"\t"+rule+"\t"+s1+" ==> "+s2);
		}
	}
	
	private static String getTXLOutput(Formula f) {
		String s=f.toStringForTXL();
		try {
			PrintWriter fout = TXL.openFileCreatingFolders(AbstractSimplifier.TEMP_FOLDER+"Formula.Mat");
			String t = f.toStringForTXL();
			fout.println(t);
			fout.flush();
			fout.close();
			Process process = Runtime.getRuntime().exec(TXL.TXL_EXECUTABLE+" -Dapply "+TXL.TXL_FOLDER+"FormulaSimpl.Txl "+AbstractSimplifier.TEMP_FOLDER+"Formula.Mat");
			if (!process.waitFor(AbstractSimplifier.TIMEOUT, TimeUnit.MILLISECONDS)) {
				process.destroyForcibly();
				return s;
			}
			BufferedReader is = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String line="", prev="";
			while ((line = is.readLine()) != null) {
				prev+=line;
			}
			if (prev.equals("")) {
				prev=s;
			}
			return prev;
		} catch (Exception e) {
			System.out.println("ERROR");
			//e.printStackTrace();
			return s;
		}

	}
	
	/*
	private static void oldMethod() {
		//String a = "((((((-(((-(-(-(-(-(-(-(-((-((-(-(((((((((-((((((((-(-e_2) & e_10) & e_0) & e_9) | e_9) | e_3) | e_6) | e_8) & e_10) | e_2) & e_9) | e_9) & e_5) & e_3) | e_4) | e_0) | e_10) | e_1) & e_4) | e_4) | e_4) | e_8) | e_9)) & e_1)))) & e_4) & e_4) & e_10) | e_2) | e_9) | e_5) | e_0) | e_6) | e_7) | e_0) & e_10)";
		//Expression<String> expr = ExprParser.parse(a.replace("-","!"));
		//System.out.println(expr.toString());
		try {
			BufferedReader fin = new BufferedReader(new FileReader("temp/transformations.txt"));
			String s="";
			for (int i=1; (s=fin.readLine())!=null; i++) {
				s=s.replace("-","!");
				while (s.contains("! !")) s=s.replace("! !", "");
				if (!s.contains("==>")) {i=0; continue;}
				String s1 = s.substring(0, s.indexOf("==>"));
				String s2 = s.substring(s.indexOf("==>")+4,s.indexOf("["));
				String rule = s.substring(s.indexOf('['));
				Formula f = new Formula(s1);
				System.out.println(i+"\t"+f.checkEquivalence(s2)+"\t"+rule+"\t"+s1+" ==> "+s2);
			}
			fin.close();
		} catch (Exception e) {e.printStackTrace();}
	}*/
}
