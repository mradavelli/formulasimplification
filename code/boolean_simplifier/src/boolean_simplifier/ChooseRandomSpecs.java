package boolean_simplifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/** Utility to randomly selecte AMOUNT_TO_SELECT formulas from ORIGIN_FILENAME and write them to OUTPUT_FILENAME */
public class ChooseRandomSpecs {
	
	private static final String ORIGIN_FILENAME="randomSpecs";
	private static final String OUTPUT_FILENAME="selected_"+ORIGIN_FILENAME;
	private static final int AMOUNT_TO_SELECT = 1000;
	
	public static void main(String[] args) {
		chooseRandomSpecs(new File(LaunchSimplifier.INPUT_FOLDER+ORIGIN_FILENAME+".txt"), new File(LaunchSimplifier.INPUT_FOLDER+OUTPUT_FILENAME+".txt"));
	}
	
	private static void chooseRandomSpecs(File input, File output) {
		try {
			int lines = LaunchSimplifier.getLinesNumber(input);
			boolean selected[] = selected(lines, AMOUNT_TO_SELECT);
			BufferedReader fin = new BufferedReader(new FileReader(input));
			PrintWriter fout = new PrintWriter(new FileWriter(output));
			String s="";
			for (int i=0; (s=fin.readLine())!=null; i++) {
				if (selected[i]) fout.println((i+1)+"\t"+s); // When reading a formula, only the text after the last tab is considered.
			}
			fin.close();
			fout.close();
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private static boolean[] selected(int length, int amountToSelect) {
		int selected=0,pos=0;
		boolean b[] = new boolean[length];
		while (selected<amountToSelect && selected<length) {
			pos=(int)(Math.random()*length);
			if (!b[pos]) { b[pos]=true; selected++; }
		}
		return b;
	}
}
