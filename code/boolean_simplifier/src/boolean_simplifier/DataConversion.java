package boolean_simplifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/** Utilities to generate input data for analysis in R */
public class DataConversion {
	public static void main(String[] args) {
		//convertOutputForR1();
		convertInputForR1();
	}
	public static final String FILENAME = "dataSimplified.csv";
	public static final String FILENAME_ORIG = "dataOriginal.csv";
	static final String R_SCRIPTS_FOLDER = "../r_scripts/";
	
	private static void convertOutputForR1() {
		try {
			PrintWriter fout = new PrintWriter(new FileWriter(R_SCRIPTS_FOLDER+FILENAME));
			fout.println("file,method,num,time,timeout,simplified,chars,op,lit,feat,nest");
			File folder = new File(LaunchSimplifier.OUTPUT_FOLDER);
			File[] listOfFiles = folder.listFiles();
			
			for (int k=0; k<LaunchSimplifier.INPUT_FILES.length; k++) {
				int minLines=-1;
				for (File f : listOfFiles) {
					if (f.isFile() && f.getName().contains(LaunchSimplifier.INPUT_FILES[k][0]+"_")) {
						int lines = LaunchSimplifier.getLinesNumber(f);
						if (minLines==-1 || lines<minLines) minLines=lines;
					}
				}
				for (File f : listOfFiles) {
					if (f.isFile() && f.getName().contains(LaunchSimplifier.INPUT_FILES[k][0]+"_")) {
						String filename = f.getName().substring(0,f.getName().lastIndexOf('_'));
						String method = f.getName().substring(f.getName().lastIndexOf('_')+1,f.getName().indexOf('.'));
						System.out.println("Outputing "+filename+"_"+method+" ("+minLines+" lines)...");
						BufferedReader fin = new BufferedReader(new FileReader(f));
						BufferedReader finput = new BufferedReader(new FileReader(LaunchSimplifier.INPUT_FOLDER+filename+".txt"));
						String s,t;
						for (int i=1; i<=minLines && (s=fin.readLine())!=null && (t=finput.readLine())!=null; i++) {
							if (i%100==0) System.out.print(i+" ");
							fout.println(filename+","+method+","+i+","+new Formula(s,Integer.parseInt(s.split("\t")[1]),s.split("\t")[2].equals("1")).getStatsFormulaMethod(new Formula(t)));
						}
						fin.close();
						finput.close();
					}
				}
				fout.flush();
			}
			fout.close();
		} catch (Exception e) {e.printStackTrace();}
	}
	private static void convertInputForR1() {
		try {
			System.out.print("File"); for (String m : LaunchSimplifier.METHODS) System.out.print(" & "+m); System.out.println(" \\\\");
			PrintWriter fout = new PrintWriter(new FileWriter(R_SCRIPTS_FOLDER+FILENAME_ORIG));
			fout.println("file,num,chars,op,lit,feat,nest,snone,satgt,sespr,sjbool,smupad,sqm,stxl");
			File folder = new File(LaunchSimplifier.OUTPUT_FOLDER);
			File[] listOfFiles = folder.listFiles();
			File[] listOfFilesInput = new File(LaunchSimplifier.INPUT_FOLDER).listFiles();
			
			for (int k=0; k<LaunchSimplifier.INPUT_FILES.length; k++) {
				int minLines=-1;
				for (File f : listOfFiles) {
					if (f.isFile() && f.getName().contains(LaunchSimplifier.INPUT_FILES[k][0]+"_")) {
						int lines = LaunchSimplifier.getLinesNumber(f);
						if (minLines==-1 || lines<minLines) minLines=lines;
					}
				}
				for (File f : listOfFilesInput) {
					if (f.isFile() && f.getName().equalsIgnoreCase(LaunchSimplifier.INPUT_FILES[k][0]+".txt")) {
						String filename = f.getName().substring(0,f.getName().lastIndexOf('.'));
						//System.out.println("Outputing "+filename+" ("+minLines+" lines)...");
						BufferedReader finput = new BufferedReader(new FileReader(LaunchSimplifier.INPUT_FOLDER+filename+".txt"));
						String t;
						BufferedReader fi[] = new BufferedReader[LaunchSimplifier.METHODS.length];
						String ti[] = new String[fi.length];
						int count[] = new int[fi.length+1];
						for (int i=0; i<fi.length; i++) try {fi[i]=new BufferedReader(new FileReader(LaunchSimplifier.OUTPUT_FOLDER+filename+"_"+LaunchSimplifier.METHODS[i]+".txt"));} catch (Exception e) {e.printStackTrace();}						
						for (int i=1; i<=minLines && (t=finput.readLine())!=null; i++) {
							for (int j=0; j<fi.length; j++) if (fi[j]!=null) ti[j]=fi[j].readLine();
							boolean simplest[] = calculateBestMethods(t, ti);
							String v="";
							for (int j=0; j<simplest.length; j++) {
								v+=","+(simplest[j]?"1":"0");
								count[j]+=(simplest[j]?1:0);
							}
							fout.println(filename+","+i+","+new Formula(t).getStatsFormula()+v);
							//if (i%100==0) System.out.print(i+" ");
						}
						finput.close();
						System.out.print(filename); for (int i=0; i<count.length; i++) System.out.print(" & "+count[i]); System.out.println(" \\\\");
					}
				}
				fout.flush();
			}
			fout.close();				
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public static boolean[] calculateBestMethods(String none, String[] simplified) {
		Formula min=new Formula(none);
		boolean simplest[] = new boolean[1+simplified.length];
		simplest[0]=true;
		for (int i=0; i<simplified.length; i++) {
			Formula f = new Formula(simplified[i]);
			int comp = f.compareTo(min);
			if (comp<=0) {
				if (comp==-1) {
					min=f;
					simplest[0]=false;
					for (int j=0; j<i; j++) simplest[j+1]=false;
				}
				simplest[i+1]=true;
			} else {
				simplest[i+1]=false;
			}
		}
		return simplest;
	}
}
