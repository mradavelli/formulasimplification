package boolean_simplifier;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.TokenSource;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.DOTTreeGenerator;
import org.antlr.stringtemplate.StringTemplate;

import bdd.BDD;
import bdd.BDDPrinter;
import boolean_simplifier.methods.AbstractSimplifier;
import boolean_simplifier.methods.execute.ExecuteATGTSimplifier;
import boolean_simplifier.parser.ASTLexer;
import boolean_simplifier.parser.ASTParser;
import graphvizjava.GraphViz;
import tgtlib.definitions.expression.Expression;
import tgtlib.definitions.expression.IdExpression;
import tgtlib.definitions.expression.parser.ExpressionParser;
import tgtlib.definitions.expression.type.EnumConstCreator;
import tgtlib.definitions.expression.visitors.IDExprCollector;

public class Formula implements Comparable<Formula> {
	private String s;
	private long time;
	private boolean timedOut;
	/** List of OR SIGNS: important is that they have decreasing length (the longest with the same prefix should compare before the shortest) */
	private static final String[] OR_SIGNS = {"or", "||", "|", "+"};
	private static final String[] AND_SIGNS = {"and", "&&", "&"};
	private static final String[] NOT_SIGNS = {"not", "!", "~", "-"};
	//private static final String[] IMPLY_SIGNS = {"->", "=>"};
	//private static final String[] COIMPLY_SIGNS = {"<->", "<=>"};
	private static final String[] EXCLUDED_LITERALS = {"not", "or", "and"};
	
	/** The simplification method */
	private String method;
	
	public Formula(String s) {
		if (s==null) s="";
		if (s.contains("\t")) s=s.substring(s.lastIndexOf("\t")+1);
		s = removeDefinedEx(s);
		s = replaceKeywords(s, Formula.generateConversionEntries("&","|","!"));
		this.s=s;
	}
	public Formula(String s, long time, boolean timedOut) {
		this(s);
		this.time=time; this.timedOut=timedOut;
	}
	public Formula(String s, String method) {
		this(s);
		this.method=method;
	}
	
	public String getMethod() {
		return method==null ? "original" : method;
	}
	public int getNumChars() {
		return s.length();
	}
	private int countOccurrences(String... keywords) {
		int count=0;
		for (int i=0; i<s.length(); i++) {
			for (String sign : keywords) {
				if (i+sign.length()<s.length() && s.substring(i,i+sign.length()).equals(sign)) {
					i+=sign.length()-1;
					count++;
				}
			}
		}
		return count;
	}
	public int getNumAnd() {
		return countOccurrences(AND_SIGNS);
	}
	public int getNumOr() {
		return countOccurrences(OR_SIGNS);
	}
	public int getNumNot() {
		return countOccurrences(NOT_SIGNS);
	}

	public int getNumImply() {
		int count=0;
		for (int i=0; i<s.length(); i++) {
			if (i>1 && i<s.length()-1 && s.substring(i, i+2).equals("->") && s.charAt(i-1)!='<') count++;
		}
		return count;
	}
	public int getNumIffs() {
		int count=0;
		for (int i=0; i<s.length(); i++) {
			if ((i<s.length()-2 && s.substring(i, i+3).equals("<->"))) count++;
		}
		return count;
	}
	public int getNumOperators() {
		return getNumAnd() + getNumOr() + getNumNot() + getNumImply() + getNumIffs();
	}
	private static boolean isLetter(char c) {
		return (c>='A' && c<='Z') || c=='_' || (c>='0' && c<='9') || (c>='a' && c<='z') ;
	}
	/** @return the list of literals with the position of the initial letter in the formula, in the order which are in the equation (repetitions are possible) */
	private ArrayList<Entry<Integer,String>> getLiterals() {
		StringBuffer sb = new StringBuffer();
		ArrayList<Entry<Integer,String>> v = new ArrayList<>();
		for (int i=0; i<s.length(); i++) {
			for (String ex : EXCLUDED_LITERALS) {
				if (i+ex.length()<s.length() && s.substring(i, i+ex.length()).equalsIgnoreCase(ex) 
					&& (i==0 || !isLetter(s.charAt(i-1)))
					&& (i+ex.length()==s.length()-1 || !isLetter(s.charAt(i+ex.length())))) {
					i+= ex.length()-1; 
					continue;
				}
			}
			if (isLetter(s.charAt(i)) && (i==0 || !isLetter(s.charAt(i-1)))) sb.append(s.charAt(i)+"");
			else if (sb.length()>0 && isLetter(s.charAt(i))) sb.append(s.charAt(i));
			if (sb.length()>0 && isLetter(s.charAt(i)) && (i==s.length()-1 || !isLetter(s.charAt(i+1)))) {
				v.add(new SimpleEntry<Integer,String>(i-sb.toString().length()+1,sb.toString()));
				sb = new StringBuffer();
			}
		}
		return v;
	}
	public int getNumLiterals() {
		return getLiterals().size();
	}
	/** @return a string containing a list of all the feature names, separated by the specified separator */
	public String getFeatureNames(String separator) {
		String s="";
		ArrayList<String> v = getFeatures();
		for (String t : v) s+=separator+t;
		return s.substring(separator.length());
	}
	/** @return the list of features (literals without repetitions), in the order which are in the equation (repetitions are NOT possible) */
	public ArrayList<String> getFeatures() {
		ArrayList<Entry<Integer,String>> a = getLiterals();
		ArrayList<String> v = new ArrayList<>();
		for (Entry<Integer,String> e : a) v.add(e.getValue());
		for (int i=0; i<v.size(); i++) {
			for (int j=i+1; j<v.size(); j++) {
				if (v.get(j).equals(v.get(i))) {
					v.remove(j);
					j--;
				}
			}
		}
		return v;
	}
	public int getNumFeatures() {
		return getFeatures().size();
	}
	/*
	public int getNumFeaturesCorrect() {
		FeatureExprParserJava parser = new FeatureExprParserJava(FeatureExprFactory.sat());
        SATFeatureExpr fexpr = (SATFeatureExpr) parser.parse(s);
        return fexpr.countDistinctFeatures();
	}
	public int getNumLiteralsCorrect() {
        return StringUtils.countMatches(s, "CONFIG_");
	}*/
	
	public int getMaxNestingLevel() {
		int level=0, maxLevel=0;
		for (int i=1; i<s.length(); i++) {
			if (s.charAt(i)=='(') {
				level++; if (level>maxLevel) maxLevel=level;
			}
			else if (s.charAt(i)==')') level--;
		}
		return maxLevel;
	}
	public String toString() {
		return s;
	}
	public String toStringWithTimeout(String separator) {
		return time+separator+(timedOut?1:0)+separator+s;
	}
	public String getStatsFormula() {
		return getNumChars()+","+getNumOperators()+","+getNumLiterals()+","+getNumFeatures()+","+getMaxNestingLevel();
	}
	public String getStatsFormulaMethod(Formula originalFormula) {
		return time+","+(timedOut?"1":"0")+","+(isSimplerThan(originalFormula)?"1":"0")+","+getStatsFormula();
	}
	
	public boolean isSimplerThan(Formula f) {
		return (getNumFeatures()<f.getNumFeatures() || (getNumFeatures()<=f.getNumFeatures() && getNumLiterals()<f.getNumLiterals()) || (getNumFeatures()<=f.getNumFeatures() && getNumLiterals()<=f.getNumLiterals() && getNumOperators()<f.getNumOperators())); // && (this.getNumChar()<=f.getNumChar() || this.getNumOperators()<=f.getNumOperators());
	}
	
	public static String removeDefinedEx(String s) {
		//String t = s.replace("definedEx(", "(");
		StringBuffer sb = new StringBuffer();
		boolean removeFirstClosedParenthesis=false;
		for (int i=0; i<s.length(); i++) {
			if (s.substring(i).indexOf("definedEx(")==0) {
				removeFirstClosedParenthesis=true;
				i=i+"definedEx(".length();
			}
			if (removeFirstClosedParenthesis && s.charAt(i)==')') {
				removeFirstClosedParenthesis=false;
			} else {
				sb.append(s.charAt(i));
			}
		}
		return sb.toString();
	}
	public static String removeParenthesis(String s) {
		String t = s;
		int opened=-1;
		for (int i=0; i<t.length()-1; i++) {
			if (t.charAt(i)=='(' && Formula.isLetter(t.charAt(i+1))) {
				opened=i;
			}
			else if (!Formula.isLetter(t.charAt(i))) opened=-1;
			if (Formula.isLetter(t.charAt(i)) && t.charAt(i+1)==')' && opened>-1) {
				t = t.substring(0,opened)+t.substring(opened+1,i+1)+(i+2<t.length() ? t.substring(i+2,t.length()) : "");
				i-=2;
				opened=-1;
			}
		}
		return t;
	}
	
	
	public String getFeatureNamesAsSingleLetters(String separator) {
		int numOfLetters = getNumFeatures();
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<numOfLetters; i++) {
			sb.append(separator+(char)('A'+i));
		}
		return sb.toString().substring(separator.length());
	}
	private ArrayList<String> getFeatureNamesOrdered() {
		ArrayList<String> a = getFeatures();
		ArrayList<String> orderedFeatures = new ArrayList<>(a.size());
		while (a.size()>0) {
			int maxI=0;
			for (int i=0; i<a.size(); i++) {
				if (a.get(i).length()>a.get(maxI).length()) maxI=i;
			}
			orderedFeatures.add(a.get(maxI));
			a.remove(maxI);
		}
		return orderedFeatures;
	}
	private String getStringWithSingleLettersFeatureNames() {
		ArrayList<String> a = getFeatureNamesOrdered();
		String s = this.s;
		for (int i=0; i<a.size(); i++) {
			s=s.replace(a.get(i), ""+(char)('A'+i));
		}
		return s;
	}
	public String codeStringForQuineMcCluskey() {
		return getStringWithSingleLettersFeatureNames().replace(" & ","").replace("!","~").replace("|", "+").replace("(","").replace(")","");
	}
	public String decodeStringFromMcCluskey(String s) {
		//System.out.println("DECODE INPUT: "+s);
		s=s.replace("\n","").replace("\t","").replace("\r","");
		StringBuilder sb = new StringBuilder();
		ArrayList<String> a = getFeatureNamesOrdered();
		for (int i=0; i<s.length(); i++) {
			if (isLetter(s.charAt(i))) {
				sb.append(a.get(s.charAt(i)-'A'));
				if (i<s.length()-1 && (isLetter(s.charAt(i+1)) || s.charAt(i+1)=='~')) sb.append(" & ");
			} else sb.append(s.charAt(i));
		}
		s=sb.toString();
		s="("+s.replace('~','!').replace("+",") | (")+")";
		s=clean(s);
		//System.out.println("DECODE OUTPUT: "+s);
		return s;
	}
	
	/** @return the list of literals, in the order which are in the equation (repetitions are possible) */
	public static String replaceKeywords(String s, ArrayList<Entry<String, String>> substitutions) {
		StringBuffer sb = new StringBuffer();
		for (int i=0; i<s.length(); i++) {
			for (Entry<String,String> e : substitutions) {
				String ex = e.getKey();
				if ( (i+ex.length()<s.length() && s.substring(i, i+ex.length()).equalsIgnoreCase(ex)) 
					&& ( !isLetter(ex.charAt(0)) || (i==0 || !isLetter(s.charAt(i-1))) )
					&& ( !isLetter(s.charAt(ex.length()-1))	|| (i+ex.length()==s.length()-1 || !isLetter(s.charAt(i+ex.length()))) ) ) {
							sb.append(" "+e.getValue()+" ");
							i+= ex.length(); 
							continue;
				}
			}
			sb.append(s.charAt(i));
		}
		return clean(sb.toString()); // removes double spaces
	}
	
	public static ArrayList<Entry<String,String>> generateConversionEntries(String wishedAndSign, String wishedOrSign, String wishedNotSign) {
		ArrayList<Entry<String,String>> replacements = new ArrayList<>();
		for (String s : AND_SIGNS) if (!s.equals(wishedAndSign)) replacements.add(new SimpleEntry<String,String>(s,wishedAndSign));
		for (String s : OR_SIGNS) if (!s.equals(wishedAndSign)) replacements.add(new SimpleEntry<String,String>(s,wishedOrSign));
		for (String s : NOT_SIGNS) if (!s.equals(wishedAndSign)) replacements.add(new SimpleEntry<String,String>(s,wishedNotSign));
		return replacements;
	}
	
	/** Format a formula for better readability in the algebraic representation,
	 * supposing that the only operator symbols are: | & ! */
	public static String clean(String s) {
		/*s=s.replace("  ", " ");
		StringBuffer sb = new StringBuffer();
		for (int i=0; i<s.length(); i++) {
			if (s.charAt(i)==' ' && ( (i>0 && s.charAt(i-1)=='!') || ( (i==0 || isUChar(s.charAt(i-1))) && (i==s.length()-1 || isUChar(s.charAt(i+1))) ) ) ) continue;
			else sb.append(s.charAt(i));
		}
		return sb.toString();*/
		return s.replace("|"," | ").replace("&", " & ").replace("  "," ").replace("( ", "(").replace(" )", ")").replace("! ","!").replace("  "," ");
	}
	/*private static boolean isUChar(char c) {
		return c=='(' || c==')' || c==' ' || c=='!';
	}*/
	
	public boolean checkEquivalence(String simplifiedFormula) {
		try {
			EnumConstCreator iec = new EnumConstCreator();
			Expression e1 = ExpressionParser.parseAsBooleanExpression(s,iec);
			Expression e2 = ExpressionParser.parseAsBooleanExpression(simplifiedFormula,iec);
			Set<IdExpression> ids = new TreeSet(e1.accept(IDExprCollector.instance));
			ids.addAll(e2.accept(IDExprCollector.instance));
			return ExecuteATGTSimplifier.checkEquivalence(ids, e1, e2);
		} catch (Exception e) {
			e.printStackTrace();
			//System.err.println("ERROR in checking equivalence "+ e.getMessage());
			return false;
		}
	}
	
	/**
	 * Checks the equivalence of this formula with the one passed as parameter
	 * @param simplifiedFormula the second formula with which we want to check the Equivalence
	 * @param timeout the maximum desired computational time (it returns -1 in case it exceeds it or in case of errors)
	 * @return 1 if equivalent, 0 if not equivalent, -1 if time exceeds or exceptions occurred.
	 */
	public int checkEquivalenceInExternalProcess(String simplifiedFormula, int timeout) {
		if (s.equals(simplifiedFormula)) return 1;
		try {
			Process process = Runtime.getRuntime().exec("java -cp .:../../libs/* boolean_simplifier.methods.ExecuteCheckEquivalence", null, new File(AbstractSimplifier.EXECUTABLE_FOLDER));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			writer.write(s, 0, s.length());
			writer.newLine();
			writer.write(simplifiedFormula, 0, simplifiedFormula.length());
			writer.newLine();
			writer.close();
			if (!process.waitFor(timeout, TimeUnit.MILLISECONDS)) { // wait for completion
				process.destroyForcibly();
				return -1;
			}
			BufferedInputStream in = new BufferedInputStream(process.getInputStream());
			byte[] bytes = new byte[4096];
			StringBuilder sb = new StringBuilder();
			while (in.read(bytes) != -1) {
				sb.append(new String(bytes, StandardCharsets.UTF_8));
			}
			String s = sb.toString();
			return Integer.parseInt(s.split("\n")[1]);
		} catch (Exception e) {System.err.println("Exception in checking equivalence");}
		return -1; // in case of error
	}
	
	/** This method is needed to create a representation compatible with TXL grammar */
	public String toStringForTXL() {
		return toStringWithParenthesis().replace('!', '-');
	}
	private String toStringWithParenthesis() {
		return clean(toStringWithParenthesis(0, s.length()));
	}
	private String toStringWithParenthesis(int start, int end) {
		int numPar=0;
		boolean addPar=false;
		boolean checked=false;
		int and=-1,or=-1;
		int a=start,b=end;
		for (int i=start; i<end; i++) {
			if (or==-1 && s.charAt(i)=='|') {
				or=i;
			}
			if (and==-1 && s.charAt(i)=='&') {
				and=i;
			}
			if (s.charAt(i)=='(') {
				numPar=1;
				if (!checked && and==-1 && or==-1) {
					for (int j=i+1; j<end; j++) {
						numPar+= (s.charAt(j)=='(')?1:((s.charAt(j)==')')?-1:0);
						if (numPar==0 && (s.charAt(j)=='&' || s.charAt(j)=='|')) {addPar=true; break;}
					}
					checked=true;
					if (!addPar) {
						numPar=0;
						a=i+1;
						b=s.substring(a, end).lastIndexOf(')')+a;
						continue;
					}
					numPar=1;
				}
				for(i=i+1; i<end; i++) {
					if (s.charAt(i)=='(') numPar++;
					else if (s.charAt(i)==')') {
						numPar--;
						if (numPar==0) break;
					}
				}
			}
		}
		//System.out.println(s.substring(start, end)+" - "+s.substring(a, b));
		if (or>-1) return (a-1>start?s.substring(start,a-1):"")+"("+ ( toStringWithParenthesis(a, or)+" | "+toStringWithParenthesis(or+1, b) ) +")";
		else if (and>-1) return (a-1>start?s.substring(start,a-1):"")+"("+ ( toStringWithParenthesis(a, and)+" & "+toStringWithParenthesis(and+1, b) ) +")";
		else return (a-1>start?s.substring(start,a):"")+s.substring(a, b)+(a-1>start?")":"");
	}
	
	public int getNumParenthesis() {
		int count=0;
		for (int i=0; i<s.length(); i++) if (s.charAt(i)==')' || s.charAt(i)=='(') count++;
		return count;
	}
	
	public static Formula readFromOutput(String filename, int method, int line) {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(LaunchSimplifier.OUTPUT_FOLDER+filename+"_"+LaunchSimplifier.METHODS[method]+".txt"));
			String s="";
			for (int i=1; (s=fin.readLine())!=null; i++) if (i==line) {
				fin.close();
				return new Formula(s.split("\t")[3], Integer.parseInt(s.split("\t")[1]), s.split("\t")[2].equals("1"));
			}
			fin.close();
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	public String flatten() {
		return clean(flatten(0,s.length(),false,false,false,0));
	}
	
	/** Flattens the formula to visualize as a Box in GraphViz. 
	 * The default and suggested as more readable format is obtained with leftToRightOr = true.
	 * 
	 * @param leftToRightOr true: OR is shown as Left->Right and AND Top->Bottom. false: AND Left->Right and OR Top->Bottom
	 * @return the string in .dot format for the input to GraphViz to make Box representation of boolean formula.
	 */
	public String flattenToBox(boolean leftToRightOr, boolean printSymbols) {
		return flatten(0,s.length(),!leftToRightOr,false,leftToRightOr,printSymbols?2:1);
	}
	
	/** Returns the flattened formula. For instance: !(A & (B & C) | (D | E)) becomes !((A & B & C) | D | E)
	 * 
	 * @param start the start index of the string
	 * @param end the end index of the string
	 * @param inand .. & (..) & .. : if it is contained in a sequence of ands
	 * @param innot !(...)         : if it is contained in a not operation
	 * @param inor  .. | (..) | .. : if it is contained in a sequence of ors
	 * @param mod  0->normalMode,  1->boxMode (to create .dot format for GraphViz Box diagram),  2->boxMode with and/ors
	 * @return the flattened String
	 */
	private String flatten(int start, int end, boolean inand, boolean innot, boolean inor, int mod) {
		int numPar=0;
		boolean addPar=false;
		boolean checked=false;
		int and=-1,or=-1;
		int a=start,b=end;
		boolean not=false;
		boolean par=false;
		for (int i=start; i<end; i++) {
			if (or==-1 && and==-1 && s.charAt(i)=='!') not=!not;
			if (or==-1 && s.charAt(i)=='|') {
				or=i;
			}
			if (and==-1 && s.charAt(i)=='&') {
				and=i;
			}
			if (s.charAt(i)=='(') {
				par=true;
				numPar=1;
				if (!checked && and==-1 && or==-1) {
					for (int j=i+1; j<end; j++) {
						numPar+= (s.charAt(j)=='(')?1:((s.charAt(j)==')')?-1:0);
						if (numPar==0 && (s.charAt(j)=='&' || s.charAt(j)=='|')) {addPar=true; break;}
					}
					checked=true;
					if (!addPar) { // the all expression is (...) or !(...)
						numPar=0;
						// remove the first and last parenthesis
						a=i+1;  // assign a
						b=s.substring(a, end).lastIndexOf(')')+a; // assign b
						break;
					}
					numPar=1;
				}
				for(i=i+1; i<end; i++) {
					if (s.charAt(i)=='(') numPar++;
					else if (s.charAt(i)==')') {
						numPar--;
						if (numPar==0) break;
					}
				}
			}
		}
		//System.out.println(s.substring(start, end)+" - "+s.substring(a, b));
		if (or>-1) {
			if (mod==0) return (innot||inand?"(":"") + flatten(a,or,false,false,true,mod)+"|"+flatten(or+1, b,false,false,true,mod)+(innot||inand?")":"");
			else return (inand?"{":"") + (innot?"!( |":"") + flatten(a,or,false,false,true,mod)+"|"+(mod==2?" ∨ ":"")+flatten(or+1, b,false,false,true,mod) + (innot?")":"") + (inand?"}":"");
		} 
		else if (and>-1) {
			if (mod==0) return (innot||inor?"(":"") + flatten(a,and,true,false,false,mod)+"&"+flatten(and+1, b,true,false,false,mod)+(innot||inor?")":"");
			else return (inor?"{":"") + (innot?"!( |":"") + flatten(a,and,true,false,false,mod)+"|"+(mod==2?" ∧ ":"")+flatten(and+1, b,true,false,false,mod) + (innot?")":"") + (inor?"}":"");
		} 
		else {
			if (not && par) return (mod==0?"!":"")+flatten(a,b,inand,true,inor,mod);
			if (par) return flatten(a,b,inand,innot,inor,mod);
			return (mod==1&&innot?"!":"")+s.substring(a,b);
		}
	}
	
	/** Indent the string:
	 * @param tab the indentation String (if null - two spaces are used)
	 * @param type the type of indentation (1-3): 
	 * 1 programming language, 
	 * 2 one variable per line,
	 * 3 brackets group on own line */
	public String indent(@Nullable String tab, int type) {
		if (tab==null) tab="  ";
		StringBuffer sb = new StringBuffer();
		String indent="";
		boolean inner=false;
		int pos=0;
		int level=0;
		for (int i=0; i<s.length(); i++) {
			if (type==1) {
				switch (s.charAt(i)) {
				case '(': sb.append("(\n"+(indent+=tab)); break;
				case ')': sb.append("\n"+(indent=indent.substring(0,indent.length()-tab.length()))+")"); break;
				default: sb.append(s.charAt(i));
				}
			} else if (type==2) {
				switch (s.charAt(i)) {
				case '(': sb.append("("); indent+=tab; break;
				case ')': sb.append("\n"+(indent=indent.substring(0,indent.length()-tab.length()))+")"); break;
				case '&':
				case '|': sb.append("\n"+indent+s.charAt(i));
						inner=false;
						for (int j=i+1; j<s.length(); j++) {
							if (!" !(".contains(""+s.charAt(j))) break;
							else if (s.charAt(j)=='(') {
								inner=true; 
								for (int k=i+1; k<=j; k++) if ("!(".contains(""+s.charAt(k))) {pos=k; break;}
								break;
							}
						}
						if (inner) {sb.append("\n"+indent); i=pos-1;} 
						break;
				default: sb.append(s.charAt(i));
				}		
			} else if (type==3) {
				switch (s.charAt(i)) {
				case '(': level++;
					inner=false;
					for (int j=i+1; j<s.length(); j++) {
						if (s.charAt(j)==')') {inner=true; break;}
						if (s.charAt(j)=='(') {inner=false; break;}
					}
					if (inner) sb.append("(");
					else {sb.append("("); } 
					break;
				case ')': level--;
					inner=false;
					for (int j=i-1; j>0; j--) {
						if (s.charAt(j)==')') {inner=false; break;}
						if (s.charAt(j)=='(') {inner=true; break;}
					}
					if (inner) sb.append(")");
					else sb.append("\n"+getIndent(tab,level)+")"); 
					break;
				case '&':
				case '|': sb.append(s.charAt(i));
						inner=false;
						for (int j=i+1; j<s.length(); j++) {
							if (!" !(".contains(""+s.charAt(j))) break;
							else if (s.charAt(j)=='(') {
								inner=true; 
								for (int k=i+1; k<=j; k++) if ("!(".contains(""+s.charAt(k))) {pos=k; break;}
								break;
							}
						}
						if (inner) {sb.append("\n"+getIndent(tab,level)); i=pos-1;}
						else {
							inner=false;
							for (int j=i-1; j>0; j--) {
								if (!(" )".contains(""+s.charAt(j)))) break;
								else if (s.charAt(j)==')') {inner=true; break;}
							}
							if (inner) sb.append("\n"+getIndent(tab,level));
						}
						break;
				default: sb.append(s.charAt(i));
				}
			}
		}
		return sb.toString();
	}
	
	private String getIndent(String tab, int level) {
		StringBuffer sb = new StringBuffer();
		for (int i=0; i<level; i++) sb.append(tab);
		return sb.toString();
	}
	
	@Override
	public int compareTo(Formula f) {
		if (isSimplerThan(f)) return -1;
		if (f.isSimplerThan(this)) return 1;
		return 0;
	}
	
	public void toBDD(File file, boolean separateNodes) {
		try {
			//Formula f = new Formula("!a || b || c");
			ArrayList<String> variables = this.getFeatureNamesOrdered();
			System.out.println(this.toStringJavaNotation());
			BDD bdd = new BDD(this.toStringJavaNotation(),variables);
			bdd.printAsTable();
			new BDDPrinter(bdd).print(file.getAbsolutePath(), separateNodes);
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public String toStringJavaNotation() {
		return s.replace("|", "||").replace("&", "&&");
	}
	
	public void toAST(File file, String format) {
		try {
			// lexer splits input into tokens
			ANTLRStringStream input = new ANTLRStringStream(s);
			TokenStream tokens = new CommonTokenStream((TokenSource) new ASTLexer(input));
			// parser generates abstract syntax tree
			ASTParser parser = new ASTParser(tokens);
			// acquire parse result
			CommonTree tree = parser.formula().getTree();
			
			DOTTreeGenerator gen = new DOTTreeGenerator();
			StringTemplate st = gen.toDOT(tree);
			GraphViz gv = new GraphViz();
	        gv.writeGraphToFile(gv.getGraph(st.toString(), format), file);
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public void toBox(File file, String format, boolean leftToRightOr, boolean printSymbols) {
		try {
			String dot = "digraph g { node[shape=record]; n1[label=\"";
			String t = this.flattenToBox(leftToRightOr, printSymbols); // OR LeftRight - AND TopBottom
			//String t = this.flatten().replace("(", "{").replace(")","}").replace("&", "|").replace("!{", "{!|");
			dot+=t+"\"];}";
			System.out.println(dot);
			GraphViz gv = new GraphViz();
	        gv.writeGraphToFile(gv.getGraph(dot, format), file);
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public void toASTAbbreviated(File file, String format) {
		try {
			String dot = toDOTForASTAbbr();
			System.out.println(dot);
			GraphViz gv = new GraphViz();
	        gv.writeGraphToFile(gv.getGraph(dot, format), file);
		} catch (Exception e) {e.printStackTrace();}		
	}
	
	private class Node {
		ArrayList<String> nodes;
		ArrayList<String> edges;
		public Node addNode(String node) { if (nodes==null) nodes=new ArrayList<String>(); nodes.add(node); return this; }
		public Node addEdge(String edge) { if (edges==null) edges=new ArrayList<String>(); edges.add(edge); return this; }
		public Node addNode(int n, String label) { addNode("n"+n+"[label=\""+label+"\"];"); return this; }
		public Node addEdge(int nodeFrom, int termFrom, int nodeTo) { addEdge("n"+nodeFrom+":f"+termFrom+" -> n"+nodeTo+";"); return this; }
		public Node addNode(Node node) { 
			if (node.nodes!=null) for (String t : node.nodes) addNode(t);
			if (node.edges!=null) for (String t : node.edges) addEdge(t);
			return this;
		}
		public String toDOT() {
			StringBuffer st = new StringBuffer();
			st.append("digraph g{ node[shape=record]; ");
			if (nodes!=null) for (String t : nodes) st.append(t);
			if (edges!=null) for (String t : edges) st.append(t);
			st.append("}");
			return st.toString();
		}
	}
	
	public String toDOTForASTAbbr() {
		Formula f = new Formula(this.flatten());
		return f.toDOTForASTAbbr(0, f.toString().length(), 1).toDOT();
	}
	
	private Node toDOTForASTAbbr(int start, int end, int n) {
		StringBuffer st = new StringBuffer();
		ArrayList<Integer> a=new ArrayList<Integer>(), b=new ArrayList<Integer>();
		int type = getTypeMain(start,end);
		char op = type==2 ? '&' : '|';
		//if (type==0) return new Node().addNode(n, s.substring(start,end));
		int ops=0, subterms=0;
		for (int i=start; i<end; i++) {
			if (s.substring(start,Math.min(end, start+2)).equals("!(") || s.charAt(i)=='(') {
				subterms++;
				int par=0;
				st.append("<f"+subterms+"> "+(s.charAt(i)=='!'?"!()":"()"));
				i+= (s.charAt(i)=='!') ? 2 : 1;
				int j=i;
				for (; j<end && par>=0; j++) {
					if (s.charAt(j)=='(') par++;
					else if (s.charAt(j)==')') par--;
				}
				a.add(i); b.add(j-1);
				i=j-1;
			}
			else if (s.charAt(i)==op) { st.append("|"); ops++; }
			else st.append(s.charAt(i));
		}
		Node node = new Node().addNode(n, (type==2?"{":"") + st.toString() + (type==2?"}":""));
		int num=n+1;
		for (int i=0; i<a.size(); i++) {
			Node subnode = toDOTForASTAbbr(a.get(i),b.get(i),num);
			node.addNode(subnode);
			node.addEdge(n, i+1, num);
			num+=subnode.nodes.size();
		}
		return node;
	}
	
	/** 
	 * @param start the start index of the formula
	 * @param end the end index
	 * @return the type of the sub-expression:
	 * -2 : !(...)
	 * -1 : (...)
	 * 0  : Single literal, also eventually negated
	 * 1  : .. | .. | .. (sequence of ors)
	 * 2  : .. & .. & .. (sequence of ands)
	 */
	private int getTypeMain(int start, int end) {
		int numPar=0;
		boolean addPar=false;
		boolean checked=false;
		int and=-1,or=-1;
		boolean not=false;
		boolean par=false;
		for (int i=start; i<end; i++) {
			if (or==-1 && and==-1 && s.charAt(i)=='!') not=!not;
			if (or==-1 && s.charAt(i)=='|') {
				or=i;
			}
			if (and==-1 && s.charAt(i)=='&') {
				and=i;
			}
			if (s.charAt(i)=='(') {
				par=true;
				numPar=1;
				if (!checked && and==-1 && or==-1) {
					for (int j=i+1; j<end; j++) {
						numPar+= (s.charAt(j)=='(')?1:((s.charAt(j)==')')?-1:0);
						if (numPar==0 && (s.charAt(j)=='&' || s.charAt(j)=='|')) {addPar=true; break;}
					}
					checked=true;
					if (!addPar) { // the all expression is (...) or !(...)
						numPar=0;
						break;
					}
					numPar=1;
				}
				for(i=i+1; i<end; i++) {
					if (s.charAt(i)=='(') numPar++;
					else if (s.charAt(i)==')') {
						numPar--;
						if (numPar==0) break;
					}
				}
			}
		}
		//System.out.println(s.substring(start, end)+" - "+s.substring(a, b));
		if (or>-1) {
			return 1;
		} 
		else if (and>-1) {
			return 2;
		} 
		else {
			if (not && par) return -2;
			if (par) return -1;
			return 0;
		}
	}
	
}
