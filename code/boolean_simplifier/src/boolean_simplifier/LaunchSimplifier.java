package boolean_simplifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import boolean_simplifier.methods.ATGT;
import boolean_simplifier.methods.AbstractSimplifier;
import boolean_simplifier.methods.Espresso;
import boolean_simplifier.methods.JBool;
import boolean_simplifier.methods.MuPAD;
import boolean_simplifier.methods.QuineMcCluskey;
import boolean_simplifier.methods.TXL;

public class LaunchSimplifier {
	
	public static final String INPUT_FOLDER = "input/";
	public static final String OUTPUT_FOLDER = "output/";
	public static final String[] METHODS = new String[] {"ATGT", "QuineMcCluskey", "Espresso", "TXL", "JBool", "MuPAD"};
	public static final String[][] INPUT_FILES = new String[][] {
		{"busyboxNestedIfDefConstraints",	"000000"}, // #358
		{"busyboxParserErrorConstraints",	"000000"}, // #23
		{"busyboxTypeErrorConstraints",		"000000"}, // #54
		{"ecosParserErrorConstraints",		"000000"}, // #133
		//{"ecosParserErrorFeatureExpr",		"00000"}, // 1 long formula
		{"ecosPreprocessorErrors",			"000000"}, // #162
		{"ecosTypeErrorConstraints",		"000000"}, // #139
		//{"ecosTypeErrorFeatureExpr",		"00000"}, // 1 long formula
		{"linuxDefUseConstraints",			"000000"}, // #19654
		{"linuxParserErrorConstraints",		"000100"}, // #8443
		//{"uclibcAllNestedConstraints", 		"00000"}, // it contains implications, that are currently not treated
		{"uclibcTypeErrorConstraints", 		"000100"}, // #947
		{"tacasSpecs",						"000100"}, // #20
		{"toughestRandomSpecs",				"000100"}, // #34
		//{"hw_java_nusmv_boolexpr_simplified",	"00000"}, // contains implications, and very very long formulas
		{"selected_randomSpecs",			"000000"}, // #1000
	};
	
	public static void executeSimplificationMethods() {		
		AbstractSimplifier as = new AbstractSimplifier();
		for (int i=0; i<INPUT_FILES.length; i++) {
			for (int j=0; j<METHODS.length; j++) {
				if (INPUT_FILES[i][1].charAt(j)=='1') {
					as = getSimplifier(j);
					String inputFilename = INPUT_FOLDER+INPUT_FILES[i][0]+".txt";
					try {
						int lines = getLinesNumber(new File(inputFilename));
						System.out.print("\n"+inputFilename+" contatins "+lines+" formulas");
						as.setSkipIntervalForPrint(10); // it prints only some formulas, to give and idea in the standard output
					} catch (Exception e) {e.printStackTrace();}
					as.simplifyAllFormulas(inputFilename, OUTPUT_FOLDER+INPUT_FILES[i][0]+"_"+METHODS[j]+".txt");		
				}
			}
		}
	}
	
	public static void main(String[] args) {
		executeSimplificationMethods();
		/*if (!isTimeout() && f2.isSimpleThan(f1)) {
			Expression e = ExpressionParser.parseAsNewBooleanExpression(s);
			CNFExpression cnf = CNFExprConverterNaive.instance.getCNFExprConverter().getCNF(e);
			// at this point id contains all the ids only once
			Dimacs dimacs = cnf.toDimacs();
			System.out.println("Dimacs\n"+cnf.toString());
			System.out.println(dimacs);
		} */
	}
	
	public static int getLinesNumber(File f) {
		try {
			/*LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(filename));
			lineNumberReader.skip(Long.MAX_VALUE);
			int lines = lineNumberReader.getLineNumber()+1;
			lineNumberReader.close();*/
			BufferedReader fin = new BufferedReader(new FileReader(f));
			int i=0;
			while (fin.readLine()!=null) i++;
			fin.close();
			return i;
		} catch (Exception e) {e.printStackTrace(); return 0;}
	}
	
	public static AbstractSimplifier getSimplifier(int i) {
		switch (i) {
		case 0: return new ATGT();
		case 1: return new QuineMcCluskey();
		case 2: return new Espresso();
		case 3: return new TXL();
		case 4: return new JBool();
		case 5: return new MuPAD();
		default: return new AbstractSimplifier();
		}
	}

	public static AbstractSimplifier getSimplifier(String method2) {
		if (method2.equals(new ATGT().getName())) return new ATGT();
		if (method2.equals(new Espresso().getName())) return new Espresso();
		if (method2.equals(new JBool().getName())) return new JBool();
		if (method2.equals(new MuPAD().getName())) return new MuPAD();
		if (method2.equals(new QuineMcCluskey().getName())) return new QuineMcCluskey();
		if (method2.equals(new TXL().getName())) return new TXL();
		return new AbstractSimplifier();
	}
}
