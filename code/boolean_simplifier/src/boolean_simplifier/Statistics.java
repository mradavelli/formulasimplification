package boolean_simplifier;

import static boolean_simplifier.DataConversion.FILENAME;
import static boolean_simplifier.DataConversion.R_SCRIPTS_FOLDER;
import static boolean_simplifier.LaunchSimplifier.INPUT_FILES;
import static boolean_simplifier.LaunchSimplifier.OUTPUT_FOLDER;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import boolean_simplifier.methods.AbstractSimplifier;

public class Statistics {
	
	// Paramaters for Analyze
	private static final String ANALYZE_FILENAME="formulasToAnalyze";
	private static String[] metrics = new String[] {"Num Chars","Num Features","Num Literals","Num \\texttt{AND}","Num \\texttt{OR}","Num \\texttt{NOT}","Max Nesting level","Num parenthesis"};
	
	// Parameters for Combine
	public static final String LOG_FILENAME = "log4";
	public static final String CONSIDERED_FILES = "100000000000";
	public static final String[] METHOD_COMBINATION = new String[] {
			"100110", // From ATGT
			"100110", // From QM
			"100110", // From Espresso
			"100110", // From TXL
			"100110", // From JBool
			"100110"  // From MuPAD
	};
	
	public static void main(String[] args) {
		//printFormulaProperties(); // analyze the selected formulas
		
		// statistics about double simplification
		//doDoubleSimplify();
		generateStats();
		//generateStats2();
		
		
	}
	
	private static void printFormulaProperties() {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(LaunchSimplifier.INPUT_FOLDER+ANALYZE_FILENAME+".txt"));
			String s="";
			while ((s=fin.readLine())!=null) if (s.contains(" ") && s.charAt(0)!='%'){
				try {
					String filename = s.split(" ")[0];
					int line = Integer.parseInt(s.substring(s.indexOf(" ")+1));
					BufferedReader in = new BufferedReader(new FileReader(LaunchSimplifier.INPUT_FOLDER+filename+".txt"));
					for (int j=1; (s=in.readLine())!=null; j++) if (j==line) {	
						String[][] a = getProperties(new Formula(s),filename,line);
						printProperties(a);
						break;
					}
					in.close();
				} catch (Exception e) {e.printStackTrace();}
			}
			fin.close();
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private static void printProperties(String[][] a) {
		for (int i=0; i<a.length; i++) {
			System.out.print(i==0?"Original":LaunchSimplifier.METHODS[i-1]);
			System.out.println(": \t"+a[i][0]);
		}
		System.out.print("\t&Orig");
		for (int i=0; i<LaunchSimplifier.METHODS.length; i++) {
			System.out.print(" & "+LaunchSimplifier.METHODS[i]);
		}
		for (int j=1; j<a[0].length; j++) {
			System.out.print("\n"+metrics[j-1]);
			for (int i=0; i<a.length; i++) {
				System.out.print(" & "+a[i][j]);
			}
		}
	}
	
	private static String[][] getProperties(Formula originalFormula, String filename, int line) {
		String[][] a = new String[LaunchSimplifier.METHODS.length+1][metrics.length+1];
		Formula f = originalFormula;
		for (int j=0; j<=LaunchSimplifier.METHODS.length; j++) {
			if (j==0) {
				f = originalFormula;
			} else {
				f = Formula.readFromOutput(filename, j-1, line);
			}
			a[j][0] = f.toString();
			a[j][1] = ""+f.getNumChars();
			a[j][2] = ""+f.getNumFeatures();
			a[j][3] = ""+f.getNumLiterals();
			a[j][4] = ""+f.getNumAnd();
			a[j][5] = ""+f.getNumOr();
			a[j][6] = ""+f.getNumNot();
			a[j][7] = ""+f.getMaxNestingLevel();
			a[j][8] = ""+f.getNumParenthesis();
		}
		return a;
	}
	
	private static void generateStats2() {
		try {
			System.out.print("Method1 / Method2 & Method1 Alone"); for (String m : LaunchSimplifier.METHODS) System.out.print(" & "+m); System.out.println(" \\\\");
			File folder = new File(LaunchSimplifier.OUTPUT_FOLDER);
			File[] listOfFiles = folder.listFiles();
			int count[][] = new int[LaunchSimplifier.METHODS.length][1+LaunchSimplifier.METHODS.length];
			
			for (int k=0; k<LaunchSimplifier.INPUT_FILES.length; k++) {
				int minLines=-1;
				for (File f : listOfFiles) {
					if (f.isFile() && f.getName().contains(LaunchSimplifier.INPUT_FILES[k][0]+"_")) {
						int lines = LaunchSimplifier.getLinesNumber(f);
						if (minLines==-1 || lines<minLines) minLines=lines;
					}
				}
				File f=new File(LaunchSimplifier.INPUT_FILES[k][0]+".txt");
				System.out.println("Outputting "+f.getName());
				String filename = f.getName().substring(0,f.getName().lastIndexOf('.'));
				BufferedReader finput = new BufferedReader(new FileReader(LaunchSimplifier.INPUT_FOLDER+filename+".txt"));
				String t;
				BufferedReader fi[] = new BufferedReader[LaunchSimplifier.METHODS.length];
				String ti[] = new String[fi.length];
				for (int i=0; i<fi.length; i++) try {fi[i]=new BufferedReader(new FileReader(LaunchSimplifier.OUTPUT_FOLDER+filename+"_"+LaunchSimplifier.METHODS[i]+".txt"));} catch (Exception e) {e.printStackTrace();}						
				for (int i=1; i<=minLines && (t=finput.readLine())!=null; i++) {
					for (int j=0; j<fi.length; j++) if (fi[j]!=null) ti[j]=fi[j].readLine();
					boolean isSimple[] = calculateWorkingMethods(t, ti);
					for (int j=0; j<isSimple.length; j++) {
						if (isSimple[j]) count[j][0]++;
						for (int c=0; c<isSimple.length; c++) {
							if (c!=j && !isSimple[j] && isSimple[c]) count[j][1+c]++;
						}
					}
				}
				finput.close();
			}
			for (int i=0; i<count.length; i++) {
				System.out.print(LaunchSimplifier.METHODS[i]);
				for (int j=0; j<count[i].length; j++) System.out.print(" & "+(count[i][j]+(j==0?0:count[i][0])));
				System.out.println(" \\\\");
			}
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private static boolean[] calculateWorkingMethods(String none, String[] simplified) {
		Formula min=new Formula(none);
		boolean isSimple[] = new boolean[simplified.length];
		for (int i=0; i<simplified.length; i++) {
			if (new Formula(simplified[i]).compareTo(min)<0) isSimple[i]=true;
		}
		return isSimple;
	}
	
	private static void generateStats() {
		String[] files = new String[] {"log1","log2","log3"};
		try {
			String s="";
			int count[][] = new int[METHOD_COMBINATION.length][METHOD_COMBINATION.length];
			for (String logfile : files) {
				BufferedReader fin = new BufferedReader(new FileReader(LaunchSimplifier.OUTPUT_FOLDER+logfile+".txt"));
				while ((s=fin.readLine())!=null) {
					for (int i=0; i<LaunchSimplifier.METHODS.length; i++) for (int j=0; j<LaunchSimplifier.METHODS.length; j++) {
						if (s.contains("_"+LaunchSimplifier.METHODS[i]) && s.contains(LaunchSimplifier.METHODS[j]+"@")) {
							count[i][j]++;
							break;
						}
					}
				}
				fin.close();
			}
			System.out.print("Method 1 / Method 2");
			for (int i=0; i<LaunchSimplifier.METHODS.length; i++) System.out.print(" & "+LaunchSimplifier.METHODS[i]);
			System.out.println();
			for (int i=0; i<LaunchSimplifier.METHODS.length; i++) {
				System.out.print(LaunchSimplifier.METHODS[i]);
				for (int j=0; j<LaunchSimplifier.METHODS.length; j++) {
					System.out.print(" & " + count[i][j]);
				}
				System.out.println(" \\\\");
			}
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private static void doDoubleSimplify() {
		try {
			String outputfilename = OUTPUT_FOLDER+LOG_FILENAME+".txt";
			PrintWriter fout = new PrintWriter(new FileWriter(outputfilename));
			for (int i=0; i<INPUT_FILES.length; i++) if (CONSIDERED_FILES.charAt(i)=='1') {
				for (int j=0; j<METHOD_COMBINATION.length; j++) for (int k=0; k<LaunchSimplifier.METHODS.length; k++) if (METHOD_COMBINATION[j].charAt(k)=='1') {
					doubleSimplify(INPUT_FILES[i][0],LaunchSimplifier.METHODS[j],LaunchSimplifier.getSimplifier(LaunchSimplifier.METHODS[k]), fout);
				}			
			}
			fout.close();
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private static void doubleSimplify(String filename, String originalMethod, AbstractSimplifier method2, PrintWriter fout) {
		int count=0;
		String inputfilename = OUTPUT_FOLDER+filename+"_"+originalMethod+".txt";
		try {
			BufferedReader fin1 = new BufferedReader(new FileReader(R_SCRIPTS_FOLDER+FILENAME));
			BufferedReader fin2 = new BufferedReader(new FileReader(inputfilename));
			String s=fin1.readLine(),t="";
			String[] st=s.split(",");
			Formula f,g;
			while ((s=fin1.readLine())!=null) {
				st=s.split(",");
				if (st[0].equals(filename) && st[1].equals(originalMethod)) {
					t=fin2.readLine();
					if (st[5].equals("1")) { // only if it was actually simplified by the method
						f = new Formula(t);
						//System.out.print(st[2]);
						g=new Formula(method2.simplify(f));
						boolean simplified=g.isSimplerThan(f);
						if (simplified) {
							fout.println(inputfilename+"_"+originalMethod+" simplified with method "+method2+"\t"+g.toString()); //+": equivalent? "+f.checkEquivalenceInExternalProcess(g.toString(), 20000));
							fout.flush();
							count++;
						}
					}
				}
			}
			fin1.close();
			fin2.close();
		} catch (Exception e) {e.printStackTrace();}
		System.out.println("Analyzed "+filename+" : "+originalMethod+" -> "+method2.getName()+" - Further simplified: "+count);
	}
}
