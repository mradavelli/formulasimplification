package boolean_simplifier.methods;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import boolean_simplifier.Formula;

/** Runs on every platform (Java based) */
public class ATGT extends AbstractSimplifier {

	@Override
	public String getName() {
		return "ATGT";
	}
	
	private static final int TIMEOUT_SLACK = 1000;
	
	@Override
	public String simplify(Formula f) {
		String s = f.toString();
		//s = "A || (A && B)";
		try {
			Process process = Runtime.getRuntime().exec("java -cp .:../../libs/* boolean_simplifier.methods.execute.ExecuteATGTSimplifier "+TIMEOUT, null, new File(EXECUTABLE_FOLDER));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			writer.write(s, 0, s.length());
			writer.newLine();
			try {writer.close();} catch (Exception ex) {}
			
			// wait for completion
			if (!process.waitFor(TIMEOUT+TIMEOUT_SLACK, TimeUnit.MILLISECONDS)) {
				process.destroyForcibly();
				hasTimedOut=true;
				return s;
				/*String t = readOutput(process, s);
				if (t.equals("ok")) {
					t = readOutput(process, s); // Demand to the process to print out the last computation achieved
					process.waitFor(TIMEOUT, TimeUnit.MILLISECONDS); // I wait maximum the TIMEOUT again (but in practice should be less than 1 second)
				}
				process.destroyForcibly();
				hasTimedOut=true;
				return (t.length()==0 ? s : t.substring(2));*/
			}
			s = readOutput(process, s);
			if (s.equals("")) {
				s = f.toString();
				hasTimedOut=true;
				System.err.println("ATGT error empty string");
			} else {			
				hasTimedOut = (s.charAt(0)=='1');
				s = s.substring(2);
			}
		} catch (Exception e) {e.printStackTrace();}
		if (new Formula(s).getNumFeatures()>f.getNumFeatures()) return f.toString();
		return s;
	}
	static String readOutput(final Process process, String s) {
		try {
			BufferedInputStream in = new BufferedInputStream(process.getInputStream());
			byte[] bytes = new byte[4096];
			StringBuilder sb = new StringBuilder();
			while (in.read(bytes) != -1) {
				sb.append(new String(bytes, StandardCharsets.UTF_8));
			}
			s = sb.toString().replace("\0","");
			s = s.substring(s.lastIndexOf("\n")+1);
			s = s.replace("not ", "!").replace(" and ", " && ").replace(" or ", " || ");
		} catch (Exception e) {e.printStackTrace();}
		return s;
	}
}
