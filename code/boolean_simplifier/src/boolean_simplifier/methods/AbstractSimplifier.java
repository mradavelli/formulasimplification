package boolean_simplifier.methods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import boolean_simplifier.Formula;

public class AbstractSimplifier {

	public static final int TIMEOUT = 10000;
	public static final int TIMEOUT_CHECK_EQUIVALENCE = 1000;
	private long startingTimestamp;
	protected boolean hasTimedOut;
	protected static final String SEPARATOR="\t";
	protected int skipIntervalForPrint;
	
	/** Temporary folder that may be used to put intermediate outputs 
	 * that may be used by any of the specific methods */
	public static final String TEMP_FOLDER = "temp/";
	public static final String EXECUTABLE_FOLDER = "bin/";
	
	public String getName() {
		return "AbstractSimplifier";
	}

	public String simplify(Formula f) {
		return f.toString();
	}
	
	protected boolean isTimeout() {
		return Calendar.getInstance().getTimeInMillis()-startingTimestamp > TIMEOUT;
	}
		
	//volatile protected String s,t;
	//volatile protected Formula f;
	public void simplifyAllFormulas(String inputFileName, String outputFileName) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(inputFileName));
			PrintWriter out = openFileCreatingFolders(outputFileName);
			String s,t;
			Formula f, fOrig;
			long msElapsed;
			int lastPrintedIndex=0;
			System.out.print("\nmethod\tnumber\ttime\ttimeout\tsimplif\tfeat\tlit\top\tnest\tequiv\tbefore\t\t\t\tafter");
			for (int i=1; (s = in.readLine()) != null; i++) {
				fOrig = new Formula(s);
				f = null;
				startingTimestamp = Calendar.getInstance().getTimeInMillis();
				hasTimedOut=false;
				t = simplify(fOrig);
				if (t.contains("\t")) {
					msElapsed = Integer.parseInt(t.split("\t")[0]);
					t = t.split("\t")[1];
				} else msElapsed = Calendar.getInstance().getTimeInMillis()-startingTimestamp;
				f = new Formula(t, msElapsed, hasTimedOut);
				out.println(i+SEPARATOR+f.toStringWithTimeout(SEPARATOR));
				out.flush();
				int isEquivalent=-1;
				//if (getName().equals("qm") || getName().equals("esp") || getName().equals("atgt")) isEquivalent=1; // for some formulas with these methods, it just takes too long and it is always true anyway
				//else 
				/*try {
					isEquivalent = fOrig.checkEquivalenceInExternalProcess(f.toString(), TIMEOUT_CHECK_EQUIVALENCE);
				} catch (Exception e) {e.printStackTrace();}*/
				if (i==1 || i-lastPrintedIndex>=skipIntervalForPrint || isEquivalent==0) {
					System.out.print("\n"+getName()+"\t"+i+"\t"+msElapsed+"\t"+(hasTimedOut?1:0)+"\t"+(f.isSimplerThan(fOrig)?1:0)+"\t"+fOrig.getNumFeatures()+"->"+f.getNumFeatures()+"\t"+fOrig.getNumLiterals()+"->"+f.getNumLiterals()+"\t"+fOrig.getNumOperators()+"->"+f.getNumOperators()+"\t"+fOrig.getMaxNestingLevel()+"->"+f.getMaxNestingLevel()+"\t"+isEquivalent+"\t"+fOrig.toString().substring(0,Math.min(fOrig.toString().length(), 30))+"\t"+f.toString().substring(0,Math.min(f.toString().length(), 30)));
					if (isEquivalent==0) System.out.print("\nORIG:\t"+fOrig.toStringForTXL()+"\nSIMP:\t"+f.toString());
					lastPrintedIndex=i;
				}
			}
			in.close();
			out.close();
		} catch (Exception e) {e.printStackTrace();}
		if (this instanceof MuPAD) MuPAD.quitProcess();
	}
	
	public static PrintWriter openFileCreatingFolders(String outputFileName) throws IOException {
		File f = new File(outputFileName);
		f.getParentFile().mkdirs();
		return new PrintWriter(new FileWriter(f));
	}

	public void setSkipIntervalForPrint(int i) {
		this.skipIntervalForPrint=i;
	}
}
