package boolean_simplifier.methods;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import boolean_simplifier.Formula;

/** Runs only on Linux */
public class Espresso extends AbstractSimplifier {
	
	@Override
	public String getName() {
		return "Espresso";
	}

	private static final String EXECUTABLE_FOLDER = "espresso/";

	/** 
	 * IMPORTANT NOTICE: To work properly, the Espresso library should be installed (the "eqntott" method should be available in the system)
	 * and espresso executable should be available in the "expresso" folder.
	 * The method writes the temporary files "formula1.eqn" and "temp.in" in the project root.
	 */
	@Override
	public String simplify(Formula f) {
		String s = f.toString();
		try {
			long startTime = Calendar.getInstance().getTimeInMillis();
			PrintWriter fout = openFileCreatingFolders(TEMP_FOLDER+"formula1.eqn");
			fout.println("NAME = formula;");
			fout.println("INORDER =" + new Formula(s).getFeatureNames(" ")+";");
			fout.println("OUTORDER = z;");
			//Expression e = ExpressionParser.parseAsNewBooleanExpression(s);
			//DNFExpression dnf = DNFExprConverter.getDNF(e); // with the current library does by-product printing in the Systemout
			//Formula f = new Formula(dnf.toString(true));
			//String formula = Formula.replaceKeywords(f.toString(), Formula.generateConversionEntries("&","|","!"));
			//fout.println("z = ("+f.toString().replace(" + ",") | (")+");");
			//String formula = Formula.replaceKeywords(s, Formula.generateConversionEntries("&","|","!"));
			fout.println("z = "+s+";");
			fout.flush();
			fout.close();
			Process process = Runtime.getRuntime().exec(new String[] {"eqntott","-l",TEMP_FOLDER+"formula1.eqn"});	        
			PrintWriter ft = openFileCreatingFolders(TEMP_FOLDER+"temp.in");
			if (!process.waitFor(TIMEOUT-(Calendar.getInstance().getTimeInMillis()-startTime), TimeUnit.MILLISECONDS)) {
				process.destroyForcibly();
				ft.close();
				hasTimedOut=true;
				return s;
			}
			BufferedReader is = new BufferedReader(new InputStreamReader(process.getInputStream()));			
			String line="";
			while ((line = is.readLine()) != null) {
				ft.println(line);
			}
			ft.flush();
			ft.close();
			is.close();
			process = Runtime.getRuntime().exec(new String[] {EXECUTABLE_FOLDER+"espresso.linux","-o","eqntott",TEMP_FOLDER+"temp.in"});	        
			if (!process.waitFor(TIMEOUT-(Calendar.getInstance().getTimeInMillis()-startTime), TimeUnit.MILLISECONDS)) {
				process.destroyForcibly();
				hasTimedOut=true;
				return s;
			}
			is = new BufferedReader(new InputStreamReader(process.getInputStream()));
			line="";
			String prev="";
			int i=0;
			while ((line = is.readLine()) != null) {
				if (i>0) prev+=line;
				i++;
			}
			is.close();
			if (prev.length()>4) prev = prev.substring(4, prev.length());
			s = Formula.clean(prev.replace(";","")); // remove the last semicolon, and the first part "z = ", which we do not need.
			if (s.equals("")) {
				System.err.println("Espresso returned empty string: possible error.");
				hasTimedOut=true;
				return f.toString();
			}
			return s;
		} catch (Exception e) {
			e.printStackTrace();
			return s;
		}
	}
}
