package boolean_simplifier.methods;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.concurrent.TimeUnit;

import boolean_simplifier.Formula;

/** Runs on every OS (Java based) */
public class JBool extends AbstractSimplifier {

	@Override
	public String getName() {
		return "JBool";
	}

	@Override
	public String simplify(Formula f) {
		String s = f.toString();
		try {
			Process process = Runtime.getRuntime().exec("java -cp .:../../libs/* boolean_simplifier.methods.execute.ExecuteJBoolSimplifier", null, new File(EXECUTABLE_FOLDER));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			writer.write(s, 0, s.length());
			writer.newLine();
			writer.close();
			if (!process.waitFor(TIMEOUT, TimeUnit.MILLISECONDS)) {
				process.destroyForcibly();
				hasTimedOut=true;
				return s;
			}
			BufferedReader is = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line="", prev="";
			while ((line = is.readLine()) != null) {
				prev=line;
			}
			if (prev.equals("")) {
				prev=s;
				System.err.println("JBool error: returned empty string.");
				hasTimedOut=true;
			}
			return prev;
		} catch (Exception e) {
			e.printStackTrace();
			return s;
		}
	}
}
