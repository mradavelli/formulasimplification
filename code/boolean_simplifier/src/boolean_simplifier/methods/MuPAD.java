package boolean_simplifier.methods;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Calendar;

import boolean_simplifier.Formula;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;

/** Runs only if MATLAB and MuPAD are installed */
public class MuPAD extends AbstractSimplifier {

	@Override
	public String getName() {
		return "MuPAD";
	}

	//private static final String MATLAB_EXECUTABLE = "/Applications/MATLAB_R2016a.app/bin/matlab";
	//private static final String MUPAD_COMMAND = "run('"+Paths.get("").toAbsolutePath().toString()+"/mupad/simplify_mupad.m');";
	
	private static final String INPUT_FILENAME = "mupad_in.txt";
	private static final String OUTPUT_FILENAME = "mupad_out.txt";
	
	private static MatlabProxyFactory factory;
	private static MatlabProxy proxy;
	private static boolean finished;
	/*private static Process process=null;
	private static volatile String lineRead="";
	private static Thread waitingReady = new Thread() {
		public void run() {
			try {
				BufferedReader is = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String s;
				while ((s=is.readLine())!=null) {
					System.out.println(s);
					if (s.contains("ready")) {lineRead=s; break;}
				}
				System.out.println("s is null "+s);
			} catch (Exception e) {e.printStackTrace();}
		}
	};
	private static Thread waitingFinished = new Thread() {
		public void run() {
			try {
				BufferedReader is = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String s;
				while ((s=is.readLine())!=null) {
					System.out.println(s);
					if (s.startsWith("finished\t")) {lineRead=s; break;}
				}
				//lineRead = is.readLine();							
				System.out.println(lineRead);
			} catch (Exception e) {e.printStackTrace();}
		}
	};*/

	@Override
	public String simplify(Formula f) {
		String s = f.toString();
		try {
			/*if (process == null || !process.isAlive()) {
				process = Runtime.getRuntime().exec(MATLAB_EXECUTABLE+" -nosplash -nodesktop -r -wait \"display('ready')\"");
				waitingReady.start();
				while (!lineRead.equals("ready")) try {Thread.sleep(100);} catch (Exception e) {e.printStackTrace();}
			}
			PrintWriter fout = new PrintWriter(new FileWriter(TEMP_FOLDER+INPUT_FILENAME));
			fout.println(s);
			fout.close();
			
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			writer.write(MUPAD_COMMAND, 0, MUPAD_COMMAND.length());
			writer.newLine();
			try {writer.close();} catch (Exception ex) {}
			
			long startingTime = Calendar.getInstance().getTimeInMillis();
			waitingFinished.start();
			while (!lineRead.startsWith("finished\t") && Calendar.getInstance().getTimeInMillis()-startingTime<TIMEOUT) try {Thread.sleep(1);} catch (Exception e) {e.printStackTrace();}
			if (lineRead.startsWith("finished\t")) {
				return lineRead.split("\t")[1];
			} else {
				process.destroyForcibly();
				process=null;
				hasTimedOut=true;
				System.out.println("Process terminated");
				return s;
			}*/
			
			s = s.replace("!", "not ");
			s = s.replace("&&", " and ");
			s = s.replace("||", " or ");
			PrintWriter fout = new PrintWriter(new FileWriter(TEMP_FOLDER+INPUT_FILENAME));
			fout.println(s);
			fout.close();
			//MatlabProxyFactory factory = new MatlabProxyFactory(new MatlabProxyFactoryOptions.Builder().setHidden(true).build());
			if (factory==null) factory = new MatlabProxyFactory();
			if (proxy==null || !proxy.isConnected()) proxy = factory.getProxy();
			long startingTime = Calendar.getInstance().getTimeInMillis();
			finished=false;
			Thread thread = new Thread() {
				public void run() {
					try {
						proxy.eval("run('"+Paths.get("").toAbsolutePath().toString()+"/mupad/simplify_mupad.m');");
						finished=true;
					} catch (Exception e) {System.err.println("Error proxy: probably due to closed Matlab");}
				}
			};
			thread.start();
			//proxy.eval("quit;");
			//proxy.disconnect();
			while (!finished && Calendar.getInstance().getTimeInMillis()-startingTime<=TIMEOUT) Thread.sleep(1);
			if (Calendar.getInstance().getTimeInMillis()-startingTime>TIMEOUT) {
				thread.interrupt();
				proxy.exit();
				proxy.disconnect();
				killMatlab();
				Thread.sleep(1000);
				hasTimedOut=true;
				return s;
			}
			BufferedReader fin = new BufferedReader(new FileReader(TEMP_FOLDER+OUTPUT_FILENAME));
			String t=fin.readLine();
			fin.close();
			return (Calendar.getInstance().getTimeInMillis()-startingTime) + "\t" + (t==null ? s : t.split("\t")[1]);
		} catch (Exception e) {
			e.printStackTrace();
			return s;
		}
	}
	
	public static void quitProcess() {
		//if (process!=null && process.isAlive()) process.destroyForcibly();
		if (proxy!=null && proxy.isConnected()) {
			try {proxy.exit();} catch (Exception e) {e.printStackTrace();}
			proxy.disconnect();
			killMatlab();
		}
	}
	
	public static void killMatlab() {
		System.out.println("Killing Matlab....");
		try {
			Runtime.getRuntime().exec("killall MATLAB_maci64");
		} catch (Exception e) {e.printStackTrace();}
	}
}
