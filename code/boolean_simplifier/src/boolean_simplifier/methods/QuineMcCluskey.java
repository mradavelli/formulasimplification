package boolean_simplifier.methods;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.concurrent.TimeUnit;

import boolean_simplifier.Formula;

/** Runs only on Linux */
public class QuineMcCluskey extends AbstractSimplifier {

	@Override
	public String getName() {
		return "QuineMcCluskey";
	}

	private static final int TIMEOUT_SLACK = 500;

	/** Works only on Linux */
	@Override
	public String simplify(Formula f) {
		String s = f.toString();
		try {
			Process process = Runtime.getRuntime().exec("java -cp .:../../libs/* boolean_simplifier.methods.execute.ExecuteQMSimplifier "+TIMEOUT, null, new File(EXECUTABLE_FOLDER));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			writer.write(s, 0, s.length());
			writer.newLine();
			try {writer.close();} catch (Exception ex) {}
			if (!process.waitFor(TIMEOUT+TIMEOUT_SLACK, TimeUnit.MILLISECONDS)) {
				process.destroyForcibly();
				hasTimedOut=true;
				return s;
			}
			BufferedReader is = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line="", prev="";
			while ((line = is.readLine()) != null) {
				prev=line;
			}
			if (prev.equals("")) {
				prev=s;
				System.err.println("QM error: returned empty string with input formula: "+s);
				hasTimedOut=true;
			} else {			
				hasTimedOut = (s.charAt(0)=='1');
				prev = prev.substring(2);
			}
			return prev;
		} catch (Exception e) {System.err.println("Exception in QuineMcCluskey output");}
		return s;
	}
}
