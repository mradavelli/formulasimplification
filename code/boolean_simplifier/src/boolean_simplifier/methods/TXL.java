package boolean_simplifier.methods;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import boolean_simplifier.Formula;

/** Runs only if TXL is installed: CHANGE THE TXL EXECUTABLE PATH IN THIS FILE! */
public class TXL extends AbstractSimplifier {

	@Override
	public String getName() {
		return "TXL";
	}

	public static final String TXL_FOLDER = "txl/input/";
	//public static final String TXL_EXECUTABLE = "txl";
	public static final String TXL_EXECUTABLE = "/usr/local/bin/txl";

	@Override
	public String simplify(Formula f) {
		String s = f.toString();
		try {
			String t = f.toStringForTXL();
			//System.out.println("\n"+s+"\n"+t);
			//if (!t.equals("")) System.exit(0);
			PrintWriter fout = openFileCreatingFolders(TEMP_FOLDER+"Formula.Mat");
			//System.out.print("\nINITIAL FORMULA:\t"+t);
			fout.println(t);
			fout.flush();
			fout.close();

			Process process = Runtime.getRuntime().exec(TXL_EXECUTABLE+" "+TXL_FOLDER+"FormulaSimpl.Txl "+TEMP_FOLDER+"Formula.Mat");

			BufferedReader is = new BufferedReader(new InputStreamReader(process.getInputStream()));
			//while ((is.readLine()) != null) {} // see http://stackoverflow.com/questions/5483830/process-waitfor-never-returns
			if (!process.waitFor(TIMEOUT, TimeUnit.MILLISECONDS)) {
				process.destroyForcibly(); // http://stackoverflow.com/questions/808276/how-to-add-a-timeout-value-when-using-javas-runtime-exec
				hasTimedOut=true;
				return s;
			}
			String line="", prev="";
			while ((line = is.readLine()) != null) {
				prev+=line;
			}
			if (prev.equals("")) {
				prev=s;
				hasTimedOut=false; // Error by TXL (usually TXL is not able to parse the whole formula because of parsing issues - formula too deep in nesting)
			}
			return Formula.clean(prev.replace("-", "!")); //.replace("| |","|").replace("& &", "&").replace("! ", "!");
		} catch (Exception e) {
			e.printStackTrace();
			return s;
		}
	}
}
