package boolean_simplifier.methods.execute;

import static extgt.coverage.fault.mutators.foms.MissingSubExpressionFault.MSF;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;

import boolean_simplifier.Formula;
import tgtlib.definitions.expression.BinaryExpression;
import tgtlib.definitions.expression.Expression;
import tgtlib.definitions.expression.IdExpression;
import tgtlib.definitions.expression.Operator;
import tgtlib.definitions.expression.parser.ExpressionParser;
import tgtlib.definitions.expression.parser.ParseException;
import tgtlib.definitions.expression.visitors.IDExprCollector;
import tgtlib.definitions.normalform.cnf.CNFExprConverterNaive;
import tgtlib.definitions.normalform.cnf.CNFExpression;
import tgtlib.definitions.normalform.cnf.Dimacs;
import tgtlib.util.Pair;

public class ExecuteATGTSimplifier {
	
	private static long timeout=-1;
	private static long startingTime;
	
	public static void main(String[] args) {
		try {
			if (args.length>0) timeout = Long.parseLong(args[0]);
			System.out.println("Type formula to be simplified with ATGT method:");
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			String s=input.readLine();
			//System.out.println(applyMutations(s));
			applyMutations(s);
		} catch (Exception e) {e.printStackTrace();}
	}
	
	/** @return the simplified expressions equivalent to es */
	private static void applyMutations(String es) throws ParseException {
		startingTime = Calendar.getInstance().getTimeInMillis();
		
		Expression e = ExpressionParser.parseAsNewBooleanExpression(es);
		// collect the variables
		Set<IdExpression> ids = e.accept(IDExprCollector.instance);

		//List<Pair<Integer, Expression>> mutations = StuckAt.STUCK_AT1.getExpressionMutator().getMutations(e);
		List<Pair<Integer, Expression>> mutations = MSF.getExpressionMutator().getMutations(e);

		//System.out.print("\nok");
		Formula f=null, simple=new Formula(es);
		for (Pair<Integer, Expression> m : mutations) {
			if (timeout>0 && Calendar.getInstance().getTimeInMillis()-startingTime >= timeout) {
				System.out.print("\n1 "+simple.toString());
				return;
			}
			// check equivalence
			if (checkEquivalence(ids,e,m.getSecond())){
				f = new Formula(m.getSecond().toString());
				if (f.isSimplerThan(simple)) {
					simple = f; 
				}
			}
		}
		System.out.print("\n0 "+simple.toString());
	}
	/** check if equivalent, they're equivalent if e1!=e2 � unsat */
	public static boolean checkEquivalence(Set<IdExpression> ids, Expression e1, Expression e2) {

		// build the solver
		ISolver solver = SolverFactory.newDefault();
		// preparing the solver
		//
		solver.reset();
		// prepare the solver to accept MAXVAR variables. MANDATORY
		// may be an underapproximation due to the new variables
		solver.newVar(ids.size());
		// not mandatory for SAT solving. MANDATORY for MAXSAT solving
		// solver.setExpectedNumberOfClauses(dimacs.getnClauses());
		// get the ids for this condition
		// TODO (it may contain more ids because when transformed to CNF, new
		// ids may be introduced)
		// solver.setExpectedNumberOfClauses(variables.size());
		// solver.setExpectedNumberOfClauses(variables.size());

		// e1 != e2
		BinaryExpression condition = BinaryExpression.mkBinExpr(e1, Operator.NEQ, e2);

		// reset the computation of the
		CNFExpression cnf = CNFExprConverterNaive.instance.getCNFExprConverter().getCNF(condition);
		// at this point id contains all the ids only once
		Dimacs dimacs = cnf.toDimacs();
		//
		try {
			IProblem problem = solver;
			// add the clauses
			for (int[] clause : dimacs.getClauses()) {
				try {
					solver.addClause(new VecInt(clause));
					// null if the model did not change????
				} catch (ContradictionException e) {
					// not a real exception
					return true;
				}
			}
			// we are done. Working now on the IProblem interface
			boolean isSatisfiable = problem.isSatisfiable();
			return !isSatisfiable;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	

	/*private static String simplify(String s) {
		try {
			for(;;){
				List<Expression> possiblesimplifications = applyMutations(s);
				if (possiblesimplifications.isEmpty()){
					return s;
				}
				// take one
				Expression newEs = possiblesimplifications.get(0);
				s = newEs.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
	
	/** @return the simplified expressions equivalent to es */
	/*private static List<Expression> applyMutations(String es) throws ParseException {
		List<Expression> eqs = new ArrayList<>();
		Expression e = ExpressionParser.parseAsNewBooleanExpression(es);
		// collect the variables
		Set<IdExpression> ids = e.accept(IDExprCollector.instance);

		//List<Pair<Integer, Expression>> mutations = StuckAt.STUCK_AT1.getExpressionMutator().getMutations(e);
		List<Pair<Integer, Expression>> mutations = MSF.getExpressionMutator().getMutations(e);
		
		for (Pair<Integer, Expression> m : mutations) {
			// check equivalence
			if (checkEquivalence(ids,e,m.getSecond())){
				eqs.add(m.getSecond());
			}
		}
		return eqs;
	}*/
}
