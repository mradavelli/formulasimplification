package boolean_simplifier.methods.execute;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import boolean_simplifier.Formula;

public class ExecuteCheckEquivalence {
	
	public static void main(String[] args) {
		try {
			System.out.println("Type the two formulas you want to check the equivalence, one at a line:");
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			String s1=input.readLine();
			String s2=input.readLine();
			//System.out.println(applyMutations(s));
			Formula f1 = new Formula(s1);
			boolean isEquivalent=f1.checkEquivalence(s2);
			System.out.println(isEquivalent?"1":"0");
		} catch (Exception e) {System.out.println("-1"); e.printStackTrace();}
	}
	
}
