package boolean_simplifier.methods.execute;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.bpodgursky.jbool_expressions.Expression;
import com.bpodgursky.jbool_expressions.parsers.ExprParser;
import com.bpodgursky.jbool_expressions.rules.RuleSet;

public class ExecuteJBoolSimplifier {
	
	public static void main(String[] args) {
		try {
			System.out.println("Type formula to be simplified with JBool method:");
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			String s=input.readLine();
			System.out.println(executeJBoolSimplifier(s));
		} catch (Exception e) {e.printStackTrace();}
	}
	
	/** @return the simplified expressions equivalent to s */
	private static String executeJBoolSimplifier(String s) {
		try {
			Expression<String> expr = ExprParser.parse(s);
			Expression<String> simplified = RuleSet.simplify(expr);
			return simplified.toString();
		} catch (Exception e) {
			e.printStackTrace(); 
			return s;
		}
	}
}
