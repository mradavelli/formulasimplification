package boolean_simplifier.methods.execute;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.bpodgursky.jbool_expressions.Expression;
import com.bpodgursky.jbool_expressions.parsers.ExprParser;
import com.bpodgursky.jbool_expressions.rules.RuleSet;

import boolean_simplifier.Formula;

public class ExecuteQMSimplifier {
	
	private static long timeout=-1;
	private static long startingTime;
	
	private static final String QM_EXECUTABLE_FOLDER = "../quinemccluskey/";
	
	public static void main(String[] args) {
		try {
			if (args.length>0) timeout = Long.parseLong(args[0]);
			System.out.println("Type formula to be simplified with QM method:");
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			String s=input.readLine();
			//System.out.println(applyMutations(s));
			System.out.println(executeQMSimplifier(s));
		} catch (Exception e) {e.printStackTrace();}
	}
	
	/** @return the simplified expressions equivalent to s */
	private static String executeQMSimplifier(String s) {
		startingTime = Calendar.getInstance().getTimeInMillis();
		try {
			Expression<String> expr = RuleSet.toDNF(ExprParser.parse(s));
			Formula f = new Formula(expr.toString());
			/*
	        Expression e = ExpressionParser.parseAsNewBooleanExpression(s); ATGT IS TOO SLOW IN DNF CONVERSION
			DNFExpression dnf = DNFExprConverter.getDNF(e); // with the current library does by-product printing in the Systemout
			f = new Formula(dnf.toString(true)); */
			if (new Formula(s).getNumFeatures()> 'Z'-'A') return "\n0 "+s; //the number of letters is not enough
			//String inputExpression = "f(A, B, C, D) = ~AB~C + A~B~C + AB~C~D + A~BC~D + BC~D + ~ABCD + A~BCD + ~A~BD";
			String inputExpression = "f(" + f.getFeatureNamesAsSingleLetters(", ") + ") = " + f.codeStringForQuineMcCluskey()+"\n";
			// Example from: http://dhruba.name/2012/10/16/java-pitfall-how-to-prevent-runtime-getruntime-exec-from-hanging/
			// and from: http://stackoverflow.com/questions/7456613/process-runtime-pass-input
			Process process = Runtime.getRuntime().exec(QM_EXECUTABLE_FOLDER + "qm -q");
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			writer.write(inputExpression, 0, inputExpression.length());
			writer.close();
			if (!process.waitFor(timeout-(Calendar.getInstance().getTimeInMillis()-startingTime), TimeUnit.MILLISECONDS)) {	// wait for completion
				process.destroyForcibly();
				kill(process);
				return "\n1 "+s;
			}
			BufferedInputStream in = new BufferedInputStream(process.getInputStream());
			byte[] bytes = new byte[4096];
			StringBuilder sb = new StringBuilder();
			while (in.read(bytes) != -1) {
				sb.append(new String(bytes, StandardCharsets.UTF_8));
			}
	
			s = sb.toString().replace("\0","");
			s=s.substring(s.lastIndexOf('=')+1);
			s = f.decodeStringFromMcCluskey(s);
			return "\n0 "+s;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "\n1 "+s;
	}
	/** Not necessarily needed, but better since some processes keep running after */
	private static void kill(Process proc) {
		try {
			Runtime rt = Runtime.getRuntime();
			Field f = proc.getClass().getDeclaredField("pid");
			f.setAccessible(true);
			int pid = f.getInt(proc);
			rt.exec("kill -9 " +pid);
		} catch (Exception e) {e.printStackTrace();}
	}

}
