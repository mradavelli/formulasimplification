package boolean_simplifier.methods.visualization;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.AbstractTableModel;

import boolean_simplifier.Formula;
import boolean_simplifier.LaunchSimplifier;
import boolean_simplifier.methods.AbstractSimplifier;

public class BoolVisualizer {

	private File file;
	private JTextArea formulaInfo, formula;
	private JTable table, simplTable;
	private FormulaTableModel tableModel = new FormulaTableModel(), simplTableModel = new FormulaTableModel();
	private JLabel statusLabel;
	
	private boolean methodAvailable[] = new boolean[] {true,false,false,true,true,false};

	private void loadFile(File f) {
		file = f;
		statusLabel.setText("Loading "+getFileStatistics().replace("\n", " - "));
		new Thread() { public void run() {
			tableModel.loadFile(file);
			tableModel.fireTableDataChanged();
			statusLabel.setText("Loaded "+getFileStatistics().replace("\n", " - "));
			table.setRowSelectionInterval(0, 0);
			formula.setText(tableModel.getFormula(0).toString());
		}}.start();
	}

	private void loadDefaultFile() {
		loadFile(new File(LaunchSimplifier.INPUT_FOLDER+LaunchSimplifier.INPUT_FILES[1][0]+".txt"));
	}

	private void createAndShowGUI() {
		try {UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());} catch (Exception e) {}
		JFrame frame = new JFrame("BoolVisualizer");
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		JMenuItem openfile = new JMenuItem("Open File");
		openfile.addActionListener(ActionListener -> {
			final JFileChooser fc = new JFileChooser("./input");
			fc.setFileFilter(new FileNameExtensionFilter("Formulas (.for, .txt)", "txt", "for"));
			int returnVal = fc.showOpenDialog(frame);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				loadFile(fc.getSelectedFile());
			}
		});
		mnFile.add(openfile);
		JMenuItem mntmEsci = new JMenuItem("Exit");
		mntmEsci.addActionListener(ActionListener -> {
			System.exit(0);
		});
		mnFile.add(mntmEsci);
		
		JMenu mnEdit = new JMenu("Edit"); menuBar.add(mnEdit);
		JMenu mntmLookFeel = new JMenu("Look&Feel");  mnEdit.add(mntmLookFeel);
		JMenuItem mntmOS = new JMenuItem("OS Default"); mntmLookFeel.add(mntmOS);
		mntmOS.addActionListener(ActionListener -> {
			try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());} catch (Exception e) {}
			SwingUtilities.updateComponentTreeUI(frame);
		});
		JMenuItem mntmMetal = new JMenuItem("Java Default"); mntmLookFeel.add(mntmMetal);
		mntmMetal.addActionListener(ActionListener -> {
			try {UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());} catch (Exception e) {}
			SwingUtilities.updateComponentTreeUI(frame); 
		});
		JMenu mnInfo = new JMenu("?"); menuBar.add(mnInfo);
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, "BoolVisualizer\n\nPart of M.Sc. Thesis by Marco Radavelli at University of Bergamo", "About", JOptionPane.PLAIN_MESSAGE);
			}
		});	mnInfo.add(mntmAbout);
		
		formula = new JTextArea(5, 20); formula.setWrapStyleWord(true); formula.setLineWrap(true);
		JScrollPane jsa1 = new JScrollPane(formula); jsa1.setBorder(BorderFactory.createTitledBorder("Selected Formula"));
		
		table=new JTable(tableModel);
		JScrollPane js1 = new JScrollPane(table); js1.setBorder(BorderFactory.createTitledBorder("Formulas in the file"));
		table.setSelectionModel(new ForcedListSelectionModel());
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setPreferredScrollableViewportSize(new Dimension(400,60));
		table.setFillsViewportHeight(true);
		table.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				JTable table =(JTable) me.getSource();
				Point p = me.getPoint();
				int row = table.rowAtPoint(p);
				if (me.getClickCount() == 2) {
					formula.setText(tableModel.getFormula(row).toString());
					formula.update(formula.getGraphics());
				}
			}
		});
		
		simplTable = new JTable(simplTableModel);
		JScrollPane js2 = new JScrollPane(simplTable); js2.setBorder(BorderFactory.createTitledBorder("Simplified Formulas"));
		simplTable.setSelectionModel(new ForcedListSelectionModel());
		simplTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		simplTable.setPreferredScrollableViewportSize(new Dimension(400,50));
		simplTable.setFillsViewportHeight(true);
		simplTable.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				JTable table =(JTable) me.getSource();
				Point p = me.getPoint();
				int row = table.rowAtPoint(p);
				if (me.getClickCount() == 2) {
					formulaInfo.setText(visualize(simplTableModel.getFormula(row)));
					formulaInfo.update(formulaInfo.getGraphics());
				}
			}
		});
		
		JButton doButton = new JButton("Perform Simplification");
		doButton.addActionListener(ActionListener -> {
			new Thread() { public void run() {
				Formula f = new Formula(formula.getText());
				ArrayList<Formula> a = new ArrayList<>();
				a.add(f);
				for (int i=0; i<LaunchSimplifier.METHODS.length; i++) if (methodAvailable[i]) {
					AbstractSimplifier as = LaunchSimplifier.getSimplifier(i);
					Formula fs = null;
					try {fs = new Formula(as.simplify(f),as.getName());} catch (Exception e) {}
					if (fs!=null) a.add(fs);
				}
				simplTableModel.setData(a);
				simplTableModel.fireTableDataChanged();
			}}.start();
		});

		formulaInfo = new JTextArea(20, 400); formulaInfo.setEditable(false);
		JScrollPane jsa2 = new JScrollPane(formulaInfo); jsa2.setBorder(BorderFactory.createTitledBorder("Elaboration results"));
		
		JPanel optionPane = new JPanel(); optionPane.setLayout(new BoxLayout(optionPane, BoxLayout.Y_AXIS));
		optionPane.add(js2);
		JPanel resultPane = new JPanel(); resultPane.setLayout(new BoxLayout(resultPane, BoxLayout.Y_AXIS)); 
		resultPane.add(jsa2);
		
		final JSplitPane split1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, resultPane, optionPane);
		split1.setResizeWeight(0.5); split1.setDividerLocation(0.5);  split1.setOneTouchExpandable(true); split1.setContinuousLayout(true);
		JPanel headpanel = new JPanel(); headpanel.setLayout(new BoxLayout(headpanel, BoxLayout.Y_AXIS));
		headpanel.add(js1); headpanel.add(jsa1); doButton.setAlignmentX(Component.CENTER_ALIGNMENT); headpanel.add(doButton); 
		JPanel mainpanel = new JPanel(); mainpanel.setLayout(new BoxLayout(mainpanel, BoxLayout.Y_AXIS));
		mainpanel.add(headpanel); split1.setAlignmentX(Component.CENTER_ALIGNMENT); mainpanel.add(split1);
		
		// create the status bar panel and shove it down the bottom of the frame
		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		frame.add(statusPanel, BorderLayout.SOUTH);
		statusPanel.setPreferredSize(new Dimension(frame.getWidth(), 16));
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		statusLabel = new JLabel("status");
		statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		statusPanel.add(statusLabel);

		frame.setVisible(true);

		frame.add(mainpanel, BorderLayout.CENTER);
		frame.pack();
		frame.setSize(800, 600);
		frame.setVisible(true);

		loadDefaultFile();				
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new BoolVisualizer().createAndShowGUI();
			}
		});
	}

	private String getFileStatistics() {
		String s = "Filename: "+file.getName();
		System.out.println(s);
		s += "\nNum. lines: "+LaunchSimplifier.getLinesNumber(file);
		System.out.println(s);
		return s;
	}

	class FormulaTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 3779944652041616754L;
		private String[] columnNames = {"num","method","chars","op","lit","feat","nest","formula"};
		private ArrayList<Formula> data = new ArrayList<>();

		public void loadFile(File f) {
			try {
				BufferedReader fin = new BufferedReader(new FileReader(f));
				String s="";
				data = new ArrayList<>();
				while ((s=fin.readLine())!=null) {
					data.add(new Formula(s));
				}
				fin.close();
			} catch (Exception e) {}
		}

		public void setData(ArrayList<Formula> data) {
			this.data=data;
		}

		@Override
		public int getRowCount() {
			return data.size();
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			Formula f = data.get(rowIndex);
			switch (columnIndex) {
			case 0: return rowIndex+1;
			case 1: return f.getMethod();
			case 2: return f.getNumChars();
			case 3: return f.getNumOperators();
			case 4: return f.getNumLiterals();
			case 5: return f.getNumFeatures();
			case 6: return f.getMaxNestingLevel();
			case 7: return f.toString().substring(0,Math.min(f.toString().length(), 40));
			default: return "";
			}
		}

		@Override
		public String getColumnName(int column) {
			return columnNames[column];
		}

		public Formula getFormula(int row) {
			return row>=0 && row<data.size() ? data.get(row) : null;
		}
	}

	public class ForcedListSelectionModel extends DefaultListSelectionModel {
		private static final long serialVersionUID = 7647092784074902100L;
		public ForcedListSelectionModel () {
			setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		@Override
		public void clearSelection() {}
		@Override
		public void removeSelectionInterval(int index0, int index1) {}
	}
	
	private String visualize(Formula f) {
		String s="";
		s+="Indentation 1:\n"+f.indent("  ", 1);
		s+="\n\nIndentation 2:\n"+f.indent("  ", 2);
		s+="\n\nIndentation 3:\n"+f.indent("  ", 3);
		try {
			File bdd1 = new File("temp/bdd1.png");
			f.toBDD(bdd1,false);
			Desktop.getDesktop().open(bdd1);
			File bdd2 = new File("temp/bdd2.png");
			f.toBDD(bdd2,true);
			Desktop.getDesktop().open(bdd2);
			File ast = new File("temp/ast.png");
			f.toAST(ast,"png");
			Desktop.getDesktop().open(ast);
			File box1 = new File("temp/box1.png");
			f.toBox(box1,"png",true,true);
			Desktop.getDesktop().open(box1);
			File box2 = new File("temp/box2.png");
			f.toBox(box2,"png",false,true);
			Desktop.getDesktop().open(box2);
			File astAbbr = new File("temp/astAbbr.png");
			f.toASTAbbreviated(astAbbr,"png");
			Desktop.getDesktop().open(astAbbr);
		} catch (Exception e) {e.printStackTrace();}
		return s;
	}
	

}