package boolean_simplifier.methods.visualization;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenSource;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;

import boolean_simplifier.Formula;
import boolean_simplifier.methods.AbstractSimplifier;
import boolean_simplifier.parser.ASTLexer;
import boolean_simplifier.parser.ASTParser;

public class GenBDD extends AbstractSimplifier {
	
	public static void main(String[] args) {
		Formula f = new Formula("A and B and C and D and (A and (A or B)) or (not(B or (A and B)))");
		System.out.println(f.toString());
		String s = new GenBDD().simplify(new Formula("A and B and C and D and (A and B)"));
		System.out.println(s);
		System.out.println(s=f.flatten());
		System.out.println(new Formula(s).indent("  ",3));
	}
	
	@Override
	public String simplify(Formula f) {
		try {
			// lexer splits input into tokens
			ANTLRStringStream input = new ANTLRStringStream(f.toString());
			TokenStream tokens = new CommonTokenStream((TokenSource) new ASTLexer(input));
			// parser generates abstract syntax tree
			ASTParser parser = new ASTParser(tokens);
			// acquire parse result
			CommonTree ast = parser.formula().getTree();
			return ast.toStringTree();
		} catch (RecognitionException e) {
			e.printStackTrace();
			return f.toString();
		}
	}
}
