grammar AST;

options{
  language=Java;
  output=AST;
  ASTLabelType = CommonTree;
}

@header { //parser
  package boolean_simplifier.parser;
}

@members { //parser
    // java code here
}

@lexer::header { //lexer
  package boolean_simplifier.parser;
}

@lexer::members {
    // java code here
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

condition: formula EOF! ;

formula 
  : disjunction (OR^ disjunction)* ;

disjunction
  : conjunction (AND^ conjunction)* ;

conjunction 
  : NOT^? (term | LPAREN! formula RPAREN!) ;

term  : TRUE | FALSE | VARIABLE ;

/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

LPAREN : '(' ;
RPAREN :  ')' ;
AND :  '&&' | '&' | 'and' | 'AND' ;
OR :  '||' | '|' | 'or' | 'OR' ;
NOT :  '!' | '~' | '-' ;
TRUE : 'true' | 'TRUE' ;
FALSE : 'false' | 'TRUE' ;
VARIABLE: CHARACTER* ;

fragment CHARACTER: ('0'..'9' | 'a'..'z' | 'A'..'Z' | '_') ;

WS : (' ' | '\t' | '\r' | '\n')+ {$channel = HIDDEN ;} ;
