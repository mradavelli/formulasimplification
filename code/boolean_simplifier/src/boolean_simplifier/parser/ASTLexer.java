// $ANTLR 3.5.2 /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g 2016-09-28 17:30:01
 //lexer
  package boolean_simplifier.parser;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ASTLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int CHARACTER=5;
	public static final int FALSE=6;
	public static final int LPAREN=7;
	public static final int NOT=8;
	public static final int OR=9;
	public static final int RPAREN=10;
	public static final int TRUE=11;
	public static final int VARIABLE=12;
	public static final int WS=13;

	    // java code here


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ASTLexer() {} 
	public ASTLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ASTLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g"; }

	// $ANTLR start "LPAREN"
	public final void mLPAREN() throws RecognitionException {
		try {
			int _type = LPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:46:8: ( '(' )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:46:10: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAREN"

	// $ANTLR start "RPAREN"
	public final void mRPAREN() throws RecognitionException {
		try {
			int _type = RPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:47:8: ( ')' )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:47:11: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAREN"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:48:5: ( '&&' | '&' | 'and' | 'AND' )
			int alt1=4;
			switch ( input.LA(1) ) {
			case '&':
				{
				int LA1_1 = input.LA(2);
				if ( (LA1_1=='&') ) {
					alt1=1;
				}

				else {
					alt1=2;
				}

				}
				break;
			case 'a':
				{
				alt1=3;
				}
				break;
			case 'A':
				{
				alt1=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}
			switch (alt1) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:48:8: '&&'
					{
					match("&&"); 

					}
					break;
				case 2 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:48:15: '&'
					{
					match('&'); 
					}
					break;
				case 3 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:48:21: 'and'
					{
					match("and"); 

					}
					break;
				case 4 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:48:29: 'AND'
					{
					match("AND"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:49:4: ( '||' | '|' | 'or' | 'OR' )
			int alt2=4;
			switch ( input.LA(1) ) {
			case '|':
				{
				int LA2_1 = input.LA(2);
				if ( (LA2_1=='|') ) {
					alt2=1;
				}

				else {
					alt2=2;
				}

				}
				break;
			case 'o':
				{
				alt2=3;
				}
				break;
			case 'O':
				{
				alt2=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}
			switch (alt2) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:49:7: '||'
					{
					match("||"); 

					}
					break;
				case 2 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:49:14: '|'
					{
					match('|'); 
					}
					break;
				case 3 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:49:20: 'or'
					{
					match("or"); 

					}
					break;
				case 4 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:49:27: 'OR'
					{
					match("OR"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:50:5: ( '!' | '~' | '-' )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:
			{
			if ( input.LA(1)=='!'||input.LA(1)=='-'||input.LA(1)=='~' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:51:6: ( 'true' | 'TRUE' )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='t') ) {
				alt3=1;
			}
			else if ( (LA3_0=='T') ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:51:8: 'true'
					{
					match("true"); 

					}
					break;
				case 2 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:51:17: 'TRUE'
					{
					match("TRUE"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:52:7: ( 'false' | 'TRUE' )
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0=='f') ) {
				alt4=1;
			}
			else if ( (LA4_0=='T') ) {
				alt4=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}

			switch (alt4) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:52:9: 'false'
					{
					match("false"); 

					}
					break;
				case 2 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:52:19: 'TRUE'
					{
					match("TRUE"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "VARIABLE"
	public final void mVARIABLE() throws RecognitionException {
		try {
			int _type = VARIABLE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:53:9: ( ( CHARACTER )* )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:53:11: ( CHARACTER )*
			{
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:53:11: ( CHARACTER )*
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( ((LA5_0 >= '0' && LA5_0 <= '9')||(LA5_0 >= 'A' && LA5_0 <= 'Z')||LA5_0=='_'||(LA5_0 >= 'a' && LA5_0 <= 'z')) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop5;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VARIABLE"

	// $ANTLR start "CHARACTER"
	public final void mCHARACTER() throws RecognitionException {
		try {
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:55:19: ( ( '0' .. '9' | 'a' .. 'z' | 'A' .. 'Z' | '_' ) )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CHARACTER"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:57:4: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:57:6: ( ' ' | '\\t' | '\\r' | '\\n' )+
			{
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:57:6: ( ' ' | '\\t' | '\\r' | '\\n' )+
			int cnt6=0;
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( ((LA6_0 >= '\t' && LA6_0 <= '\n')||LA6_0=='\r'||LA6_0==' ') ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt6 >= 1 ) break loop6;
					EarlyExitException eee = new EarlyExitException(6, input);
					throw eee;
				}
				cnt6++;
			}

			_channel = HIDDEN ;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:8: ( LPAREN | RPAREN | AND | OR | NOT | TRUE | FALSE | VARIABLE | WS )
		int alt7=9;
		switch ( input.LA(1) ) {
		case '(':
			{
			alt7=1;
			}
			break;
		case ')':
			{
			alt7=2;
			}
			break;
		case '&':
			{
			alt7=3;
			}
			break;
		case 'a':
			{
			int LA7_4 = input.LA(2);
			if ( (LA7_4=='n') ) {
				int LA7_15 = input.LA(3);
				if ( (LA7_15=='d') ) {
					int LA7_22 = input.LA(4);
					if ( ((LA7_22 >= '0' && LA7_22 <= '9')||(LA7_22 >= 'A' && LA7_22 <= 'Z')||LA7_22=='_'||(LA7_22 >= 'a' && LA7_22 <= 'z')) ) {
						alt7=8;
					}

					else {
						alt7=3;
					}

				}

				else {
					alt7=8;
				}

			}

			else {
				alt7=8;
			}

			}
			break;
		case 'A':
			{
			int LA7_5 = input.LA(2);
			if ( (LA7_5=='N') ) {
				int LA7_16 = input.LA(3);
				if ( (LA7_16=='D') ) {
					int LA7_23 = input.LA(4);
					if ( ((LA7_23 >= '0' && LA7_23 <= '9')||(LA7_23 >= 'A' && LA7_23 <= 'Z')||LA7_23=='_'||(LA7_23 >= 'a' && LA7_23 <= 'z')) ) {
						alt7=8;
					}

					else {
						alt7=3;
					}

				}

				else {
					alt7=8;
				}

			}

			else {
				alt7=8;
			}

			}
			break;
		case '|':
			{
			alt7=4;
			}
			break;
		case 'o':
			{
			int LA7_7 = input.LA(2);
			if ( (LA7_7=='r') ) {
				int LA7_17 = input.LA(3);
				if ( ((LA7_17 >= '0' && LA7_17 <= '9')||(LA7_17 >= 'A' && LA7_17 <= 'Z')||LA7_17=='_'||(LA7_17 >= 'a' && LA7_17 <= 'z')) ) {
					alt7=8;
				}

				else {
					alt7=4;
				}

			}

			else {
				alt7=8;
			}

			}
			break;
		case 'O':
			{
			int LA7_8 = input.LA(2);
			if ( (LA7_8=='R') ) {
				int LA7_18 = input.LA(3);
				if ( ((LA7_18 >= '0' && LA7_18 <= '9')||(LA7_18 >= 'A' && LA7_18 <= 'Z')||LA7_18=='_'||(LA7_18 >= 'a' && LA7_18 <= 'z')) ) {
					alt7=8;
				}

				else {
					alt7=4;
				}

			}

			else {
				alt7=8;
			}

			}
			break;
		case '!':
		case '-':
		case '~':
			{
			alt7=5;
			}
			break;
		case 't':
			{
			int LA7_10 = input.LA(2);
			if ( (LA7_10=='r') ) {
				int LA7_19 = input.LA(3);
				if ( (LA7_19=='u') ) {
					int LA7_24 = input.LA(4);
					if ( (LA7_24=='e') ) {
						int LA7_27 = input.LA(5);
						if ( ((LA7_27 >= '0' && LA7_27 <= '9')||(LA7_27 >= 'A' && LA7_27 <= 'Z')||LA7_27=='_'||(LA7_27 >= 'a' && LA7_27 <= 'z')) ) {
							alt7=8;
						}

						else {
							alt7=6;
						}

					}

					else {
						alt7=8;
					}

				}

				else {
					alt7=8;
				}

			}

			else {
				alt7=8;
			}

			}
			break;
		case 'T':
			{
			int LA7_11 = input.LA(2);
			if ( (LA7_11=='R') ) {
				int LA7_20 = input.LA(3);
				if ( (LA7_20=='U') ) {
					int LA7_25 = input.LA(4);
					if ( (LA7_25=='E') ) {
						int LA7_28 = input.LA(5);
						if ( ((LA7_28 >= '0' && LA7_28 <= '9')||(LA7_28 >= 'A' && LA7_28 <= 'Z')||LA7_28=='_'||(LA7_28 >= 'a' && LA7_28 <= 'z')) ) {
							alt7=8;
						}

						else {
							alt7=6;
						}

					}

					else {
						alt7=8;
					}

				}

				else {
					alt7=8;
				}

			}

			else {
				alt7=8;
			}

			}
			break;
		case 'f':
			{
			int LA7_12 = input.LA(2);
			if ( (LA7_12=='a') ) {
				int LA7_21 = input.LA(3);
				if ( (LA7_21=='l') ) {
					int LA7_26 = input.LA(4);
					if ( (LA7_26=='s') ) {
						int LA7_29 = input.LA(5);
						if ( (LA7_29=='e') ) {
							int LA7_31 = input.LA(6);
							if ( ((LA7_31 >= '0' && LA7_31 <= '9')||(LA7_31 >= 'A' && LA7_31 <= 'Z')||LA7_31=='_'||(LA7_31 >= 'a' && LA7_31 <= 'z')) ) {
								alt7=8;
							}

							else {
								alt7=7;
							}

						}

						else {
							alt7=8;
						}

					}

					else {
						alt7=8;
					}

				}

				else {
					alt7=8;
				}

			}

			else {
				alt7=8;
			}

			}
			break;
		case '\t':
		case '\n':
		case '\r':
		case ' ':
			{
			alt7=9;
			}
			break;
		default:
			alt7=8;
		}
		switch (alt7) {
			case 1 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:10: LPAREN
				{
				mLPAREN(); 

				}
				break;
			case 2 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:17: RPAREN
				{
				mRPAREN(); 

				}
				break;
			case 3 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:24: AND
				{
				mAND(); 

				}
				break;
			case 4 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:28: OR
				{
				mOR(); 

				}
				break;
			case 5 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:31: NOT
				{
				mNOT(); 

				}
				break;
			case 6 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:35: TRUE
				{
				mTRUE(); 

				}
				break;
			case 7 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:40: FALSE
				{
				mFALSE(); 

				}
				break;
			case 8 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:46: VARIABLE
				{
				mVARIABLE(); 

				}
				break;
			case 9 :
				// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:1:55: WS
				{
				mWS(); 

				}
				break;

		}
	}



}
