// $ANTLR 3.5.2 /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g 2016-09-28 17:30:01
 //parser
  package boolean_simplifier.parser;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class ASTParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "CHARACTER", "FALSE", "LPAREN", 
		"NOT", "OR", "RPAREN", "TRUE", "VARIABLE", "WS"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int CHARACTER=5;
	public static final int FALSE=6;
	public static final int LPAREN=7;
	public static final int NOT=8;
	public static final int OR=9;
	public static final int RPAREN=10;
	public static final int TRUE=11;
	public static final int VARIABLE=12;
	public static final int WS=13;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public ASTParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public ASTParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return ASTParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g"; }

	 //parser
	    // java code here


	public static class condition_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "condition"
	// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:29:1: condition : formula EOF !;
	public final ASTParser.condition_return condition() throws RecognitionException {
		ASTParser.condition_return retval = new ASTParser.condition_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token EOF2=null;
		ParserRuleReturnScope formula1 =null;

		CommonTree EOF2_tree=null;

		try {
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:29:10: ( formula EOF !)
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:29:12: formula EOF !
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_formula_in_condition70);
			formula1=formula();
			state._fsp--;

			adaptor.addChild(root_0, formula1.getTree());

			EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_condition72); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "condition"


	public static class formula_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "formula"
	// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:31:1: formula : disjunction ( OR ^ disjunction )* ;
	public final ASTParser.formula_return formula() throws RecognitionException {
		ASTParser.formula_return retval = new ASTParser.formula_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token OR4=null;
		ParserRuleReturnScope disjunction3 =null;
		ParserRuleReturnScope disjunction5 =null;

		CommonTree OR4_tree=null;

		try {
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:32:3: ( disjunction ( OR ^ disjunction )* )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:32:5: disjunction ( OR ^ disjunction )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_disjunction_in_formula85);
			disjunction3=disjunction();
			state._fsp--;

			adaptor.addChild(root_0, disjunction3.getTree());

			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:32:17: ( OR ^ disjunction )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==OR) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:32:18: OR ^ disjunction
					{
					OR4=(Token)match(input,OR,FOLLOW_OR_in_formula88); 
					OR4_tree = (CommonTree)adaptor.create(OR4);
					root_0 = (CommonTree)adaptor.becomeRoot(OR4_tree, root_0);

					pushFollow(FOLLOW_disjunction_in_formula91);
					disjunction5=disjunction();
					state._fsp--;

					adaptor.addChild(root_0, disjunction5.getTree());

					}
					break;

				default :
					break loop1;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "formula"


	public static class disjunction_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "disjunction"
	// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:34:1: disjunction : conjunction ( AND ^ conjunction )* ;
	public final ASTParser.disjunction_return disjunction() throws RecognitionException {
		ASTParser.disjunction_return retval = new ASTParser.disjunction_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token AND7=null;
		ParserRuleReturnScope conjunction6 =null;
		ParserRuleReturnScope conjunction8 =null;

		CommonTree AND7_tree=null;

		try {
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:35:3: ( conjunction ( AND ^ conjunction )* )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:35:5: conjunction ( AND ^ conjunction )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_conjunction_in_disjunction104);
			conjunction6=conjunction();
			state._fsp--;

			adaptor.addChild(root_0, conjunction6.getTree());

			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:35:17: ( AND ^ conjunction )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==AND) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:35:18: AND ^ conjunction
					{
					AND7=(Token)match(input,AND,FOLLOW_AND_in_disjunction107); 
					AND7_tree = (CommonTree)adaptor.create(AND7);
					root_0 = (CommonTree)adaptor.becomeRoot(AND7_tree, root_0);

					pushFollow(FOLLOW_conjunction_in_disjunction110);
					conjunction8=conjunction();
					state._fsp--;

					adaptor.addChild(root_0, conjunction8.getTree());

					}
					break;

				default :
					break loop2;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "disjunction"


	public static class conjunction_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "conjunction"
	// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:37:1: conjunction : ( NOT ^)? ( term | LPAREN ! formula RPAREN !) ;
	public final ASTParser.conjunction_return conjunction() throws RecognitionException {
		ASTParser.conjunction_return retval = new ASTParser.conjunction_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token NOT9=null;
		Token LPAREN11=null;
		Token RPAREN13=null;
		ParserRuleReturnScope term10 =null;
		ParserRuleReturnScope formula12 =null;

		CommonTree NOT9_tree=null;
		CommonTree LPAREN11_tree=null;
		CommonTree RPAREN13_tree=null;

		try {
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:38:3: ( ( NOT ^)? ( term | LPAREN ! formula RPAREN !) )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:38:5: ( NOT ^)? ( term | LPAREN ! formula RPAREN !)
			{
			root_0 = (CommonTree)adaptor.nil();


			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:38:8: ( NOT ^)?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==NOT) ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:38:8: NOT ^
					{
					NOT9=(Token)match(input,NOT,FOLLOW_NOT_in_conjunction124); 
					NOT9_tree = (CommonTree)adaptor.create(NOT9);
					root_0 = (CommonTree)adaptor.becomeRoot(NOT9_tree, root_0);

					}
					break;

			}

			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:38:11: ( term | LPAREN ! formula RPAREN !)
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==FALSE||(LA4_0 >= TRUE && LA4_0 <= VARIABLE)) ) {
				alt4=1;
			}
			else if ( (LA4_0==LPAREN) ) {
				alt4=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}

			switch (alt4) {
				case 1 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:38:12: term
					{
					pushFollow(FOLLOW_term_in_conjunction129);
					term10=term();
					state._fsp--;

					adaptor.addChild(root_0, term10.getTree());

					}
					break;
				case 2 :
					// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:38:19: LPAREN ! formula RPAREN !
					{
					LPAREN11=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_conjunction133); 
					pushFollow(FOLLOW_formula_in_conjunction136);
					formula12=formula();
					state._fsp--;

					adaptor.addChild(root_0, formula12.getTree());

					RPAREN13=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_conjunction138); 
					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "conjunction"


	public static class term_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "term"
	// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:40:1: term : ( TRUE | FALSE | VARIABLE );
	public final ASTParser.term_return term() throws RecognitionException {
		ASTParser.term_return retval = new ASTParser.term_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token set14=null;

		CommonTree set14_tree=null;

		try {
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:40:7: ( TRUE | FALSE | VARIABLE )
			// /Users/marcoradavelli/Documents/workspace/formulasimplification/code/boolean_simplifier/src/boolean_simplifier/parser/AST.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set14=input.LT(1);
			if ( input.LA(1)==FALSE||(input.LA(1) >= TRUE && input.LA(1) <= VARIABLE) ) {
				input.consume();
				adaptor.addChild(root_0, (CommonTree)adaptor.create(set14));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "term"

	// Delegated rules



	public static final BitSet FOLLOW_formula_in_condition70 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_condition72 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_disjunction_in_formula85 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_OR_in_formula88 = new BitSet(new long[]{0x00000000000019C0L});
	public static final BitSet FOLLOW_disjunction_in_formula91 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_conjunction_in_disjunction104 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_AND_in_disjunction107 = new BitSet(new long[]{0x00000000000019C0L});
	public static final BitSet FOLLOW_conjunction_in_disjunction110 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_NOT_in_conjunction124 = new BitSet(new long[]{0x00000000000018C0L});
	public static final BitSet FOLLOW_term_in_conjunction129 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_conjunction133 = new BitSet(new long[]{0x00000000000019C0L});
	public static final BitSet FOLLOW_formula_in_conjunction136 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_RPAREN_in_conjunction138 = new BitSet(new long[]{0x0000000000000002L});
}
