% Simplify Mates logic formulas
include "GrammarFormula.Grm"

function main
    replace [program] 
        P [formula]
    by
        P %[adaptnotation]
          [normalize]
          %[findpatterns]
          [simplify]
          %[findpatterns]
          [denormalize]
          %[textify]
          %[deadaptnotation]
end function

%include "AdaptNotation.Rul"
%include "FindPatterns.Rul"
include "Normalize.Rul"
include "Simplify.Rul"
include "Denormalize.Rul"
%include "Textify.Rul"
%include "DeAdaptNotation.Rul"