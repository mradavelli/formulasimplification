% Very useful example: http://www.program-transformation.org/Sts/GotoEliminationUsingTXL

include "GrammarFormula2.grm"

function main
    replace [program] 
        P [program]
    by
        P [simplify]
end function

rule simplify
	replace [formula]
		S [formula]
	construct NewS [formula]
		S [t1]		%redundancy%
	where not 
		NewS [=S]
	by
		NewS
end rule

rule t1
	replace [repeat disjunctions]
		A [disjunctions] A Rest [repeat disjunctions]
	by
		A Rest
end rule

%rule t2
%	replace [repeat conjunctions]
%		A [conjunctions] Rest [repeat conjunctions]
%	skipping [conjunctions]
%	deconstruct * Rest
%		A FinalRest [repeat conjunctions]
%	by
%		A FinalRest
%end rule