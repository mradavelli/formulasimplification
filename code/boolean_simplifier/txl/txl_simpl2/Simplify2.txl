include "GrammarFormula2.grm"

function main
    replace [program] 
        P [program]
    by
        P [simplify]
end function

rule simplify
	replace [formula]
		S [formula]
	construct NewS [formula]
		S [t3][t3b]		%redundancy
		
	where not 
		NewS [=S]
	by
		NewS
end rule

rule t1
	replace [repeat conjunctions]
		A [conjunctions] A Rest [repeat conjunctions]
	by
		A Rest
end rule

rule t1b
	replace [formula]
		A [conjunction] B [conjunctions] Rest [repeat conjunctions]
	deconstruct * [conjunction] B
		A
	by
		A Rest
end rule

rule t2
	replace [repeat negations]
		A [negations] A Rest [repeat negations]
	by
		A Rest
end rule

rule t2b
	replace [conjunction]
		A [negation] B [negations] Rest [repeat negations]
	deconstruct * [negation] B
		A
	by
		A Rest
end rule

rule t3
	replace [repeat conjunctions]
		A [conjunctions] Rest [repeat conjunctions]
	skipping [conjunctions]
	deconstruct * Rest
		A FinalRest [repeat conjunctions]
	by
		A FinalRest
end rule

rule t3b
	replace [formula]
		A [conjunction] B [conjunctions] Rest [repeat conjunctions]
	deconstruct * [conjunction] B
		A
	by
		A Rest
end rule

rule t4
	replace [repeat conjunctions]
		
	by
		
end rule
